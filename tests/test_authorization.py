"""Test authorization module."""
import time
from unittest import mock

import django.contrib.auth.models as auth_models
from django.test import override_settings

from datawarehouse import authorization
from datawarehouse import models
from tests import utils


class TestPolicyAuthorizationBackend(utils.TestCase):
    """Test PolicyAuthorizationBackend authorization module."""

    fixtures = [
        'tests/fixtures/authorization.yaml'
    ]

    def test_is_authorized_method(self):
        """
        Test is_authorized_for_policy.

        Given 2 different checkouts and 2 authorizations, give the user a mix of them and
        check that it can read one and write the other.
        """
        session = self.client.session
        session['user_groups'] = [
            auth_models.Group.objects.get(name='group_1').id,
            auth_models.Group.objects.get(name='group_2').id
        ]
        session.save()

        cases = (
            ('policy-r1-w3', 'read', True),
            ('policy-r1-w3', 'write', False),
            ('policy-r3-w2', 'read', False),
            ('policy-r3-w2', 'write', True),
        )

        for policy_name, method, authorized in cases:
            policy = models.Policy.objects.get(name=policy_name)
            self.assertEqual(
                authorization.PolicyAuthorizationBackend.is_authorized_for_policy(
                    self.client, policy, method
                ),
                authorized,
                f'{policy_name} - {method} - {authorized}',
            )

        # Ensure that an undefined method raises an exception.
        self.assertRaises(
            Exception,
            authorization.PolicyAuthorizationBackend.is_authorized_for_policy, self.client, policy, 'foobar'
        )

    @mock.patch('datawarehouse.authorization.PolicyAuthorizationBackend.filter_authorized')
    def test_all_objects_authorized(self, filter_authorized):
        """Test all_objects_authorized."""
        queryset = mock.Mock()
        queryset.count.return_value = 1
        filter_authorized.return_value = queryset

        test_queryset = mock.Mock()

        # queryset.count != test_queryset.count
        test_queryset.count.return_value = 2
        self.assertFalse(
            authorization.PolicyAuthorizationBackend.all_objects_authorized(
                mock.Mock(), test_queryset
            )
        )

        # queryset.count == test_queryset.count
        test_queryset.count.return_value = 1
        self.assertTrue(
            authorization.PolicyAuthorizationBackend.all_objects_authorized(
                mock.Mock(), test_queryset
            )
        )

    def test_get_users_authorized(self):
        """Test get_users_authorized with read and write methods."""
        all_users = auth_models.User.objects.values_list('username', flat=True)

        cases = (
            ('issue_1', {'read': ['user_g1', 'user_g1_g3', 'user_su'],
                         'write': ['user_g1_g3', 'user_g3', 'user_su']}),
            ('issue_2', {'read': all_users,
                         'write': all_users}),
            ('issue_3', {'read': [], 'write': []}),
        )

        for issue_description, users in cases:
            issue = models.Issue.objects.get(description=issue_description)

            self.assertCountEqual(
                [user.username for user in issue.users_read_authorized],
                users['read']
            )

            self.assertCountEqual(
                [user.username for user in issue.users_write_authorized],
                users['write']
            )

    @mock.patch('datawarehouse.models.Test.path_to_policy', None)
    def test_get_users_authorized_no_policy(self):
        """
        Test get_users_authorized with read and write methods.

        Model has no path_to_policy.
        """
        test = models.Test.objects.get(name="foobar")
        all_users = auth_models.User.objects.values_list('username', flat=True)

        self.assertCountEqual(
            [user.username for user in test.users_read_authorized],
            all_users
        )

        self.assertCountEqual(
            [user.username for user in test.users_write_authorized],
            all_users
        )

    def test_get_users_authorized_mixed_policies(self):
        """
        Test get_users_authorized with read and write methods.

        Ensure objects linked with mixed policies (some public, some without policy)
        return the correct value for _is_authorized.
        """
        git_tree = models.GitTree.objects.get(name='test_tree')

        for method in ('read', 'write'):
            users = authorization.PolicyAuthorizationBackend.get_users_authorized(
                        git_tree, method
                    )
            self.assertCountEqual(
                ['user_g1', 'user_g3', 'user_g1_g3', 'user_su'],
                [user.username for user in users]
            )

        auth_models.User.objects.all().delete()

        for method in ('read', 'write'):
            self.assertQuerysetEqual(
                authorization.PolicyAuthorizationBackend.get_users_authorized(
                    git_tree, method
                ), [],
            )

    def test_get_policies_authorized(self):
        # pylint: disable=protected-access
        """
        Test get_policies_authorized.

        Check that get_policies_authorized returns the correct policies for the user.
        """
        session = self.client.session
        session['user_groups'] = [
            auth_models.Group.objects.get(name='group_1').id,
            auth_models.Group.objects.get(name='group_2').id
        ]
        session.save()

        read_policies = authorization.PolicyAuthorizationBackend._get_policies_authorized(self.client, 'read')
        self.assertEqual(
            set(['policy-ra-wa', 'policy-r1-w3', 'policy-r1-w1']),
            set(p.name for p in read_policies)
        )

        write_policies = authorization.PolicyAuthorizationBackend._get_policies_authorized(self.client, 'write')
        self.assertEqual(
            set(['policy-ra-wa', 'policy-r3-w2', 'policy-r1-w1', 'policy-r3-w1']),
            set(p.name for p in write_policies)
        )

    @staticmethod
    @mock.patch('datawarehouse.authorization.PolicyAuthorizationBackend._get_policies_authorized')
    def test_get_policies_read_authorized(get_policies_authorized):
        """Test get_policies_read_authorized calls _get_policies_authorized correctly."""
        request = mock.Mock()
        authorization.PolicyAuthorizationBackend.get_policies_read_authorized(request)
        get_policies_authorized.assert_called_with(request, 'read')

    @staticmethod
    @mock.patch('datawarehouse.authorization.PolicyAuthorizationBackend._get_policies_authorized')
    def test_get_policies_write_authorized(get_policies_authorized):
        """Test get_policies_write_authorized calls _get_policies_authorized correctly."""
        request = mock.Mock()
        authorization.PolicyAuthorizationBackend.get_policies_write_authorized(request)
        get_policies_authorized.assert_called_with(request, 'write')

    def test_filter_authorized_multiple_policies(self):
        # pylint: disable=protected-access
        """
        Test _filter_authorized when object is linked to multiple policies.

        path_to_policy value could go through a one-to-many relationship to
        reach a policy. In this case, it's possible to be in a situation where an
        object is linked to multiple policies.

        Make sure the object is authorized if the user is authorized to at least one
        of the linked policies.

        In this test:
            - checkout_1 can be read by the user
            - checkout_2 can't be read by the user
            - checkout_3 can't be read by anyone
        Therefore:
            - test_tree can be read by the user thanks to checkout_1
        """
        git_tree = models.GitTree.objects.get(name='test_tree')

        session = self.client.session
        session['user_groups'] = []
        session.save()

        for method in ('read', 'write'):
            self.assertQuerysetEqual(
                [],
                authorization.PolicyAuthorizationBackend._filter_authorized(
                    self.client, models.GitTree.objects.all(), method
                )
            )

        session['user_groups'] = [auth_models.Group.objects.get(name='group_1').id]
        session.save()

        for method in ('read', 'write'):
            self.assertQuerysetEqual(
                authorization.PolicyAuthorizationBackend._filter_authorized(
                    self.client, models.GitTree.objects.all(), method
                ),
                [git_tree]
            )

    def test_filter_authorized_single_policy(self):
        # pylint: disable=protected-access
        """
        Test _filter_authorized when object is linked to a single policy.

        path_to_policy value is normally linked to a single policy.

        Make sure the object is authorized if the user is authorized to the
        linked policy.

        In this test:
            - checkout_1 can be read by the user
            - checkout_2 can't be read by the user
            - checkout_3 can't be read by anyone
        """
        session = self.client.session
        session['user_groups'] = [auth_models.Group.objects.get(name='group_1').id]
        session.save()

        for method in ('read', 'write'):
            self.assertQuerysetEqual(
                authorization.PolicyAuthorizationBackend._filter_authorized(
                    self.client, models.KCIDBCheckout.objects.all(), method
                ),
                models.KCIDBCheckout.objects.filter(id='checkout_1')
            )


class TestRequestAuthorization(utils.TestCase):
    """Test RequestAuthorization middleware."""

    @mock.patch('datawarehouse.authorization.RequestAuthorization.fill_user_data')
    def test_call(self, fill_user_data):
        """
        Test __call__ calls to fill_user_data *before* calling get_response.

        Check that the request parameter for get_response call contains the
        modified data by fill_user_data.
        """
        def _fill_user_data_mock(request):
            request.session['dummy_data'] = {'foo': 'bar'}

        class GetResponseMock:
            # pylint: disable=too-few-public-methods
            """Mock get_response."""

            def __init__(self, testcase):
                """Save parent self to call assertEqual."""
                self.testcase = testcase

            def __call__(self, request):
                """Look for the dummy_data in the session."""
                self.testcase.assertEqual(
                    request.session['dummy_data'],
                    {'foo': 'bar'}
                )

        fill_user_data.side_effect = _fill_user_data_mock

        request = mock.Mock()
        request.session = {}
        request.META = {}

        authorization.RequestAuthorization(GetResponseMock(self))(request)
        self.assertTrue(fill_user_data.called)
        fill_user_data.assert_called_with(request)

    @mock.patch('datawarehouse.authorization.PolicyAuthorizationBackend.get_user_groups')
    def test_fill_user_data_first_request(self, get_user_groups):
        """
        Test that fill_user_data updates the information on log in/out.

        First anonymous request updates user_id and calls get_user_groups.
        """
        request = mock.Mock()
        request.session = {}
        request.META = {}

        anonymous = auth_models.AnonymousUser()

        request.user = anonymous
        authorization.RequestAuthorization.fill_user_data(request)
        self.assertEqual('anonymous', request.session.get('user_id'))
        get_user_groups.assert_called_with(request)

    @mock.patch('datawarehouse.authorization.PolicyAuthorizationBackend.get_user_groups')
    def test_fill_user_data_next_anon_request(self, get_user_groups):
        """
        Test that fill_user_data updates the information on log in/out.

        Following anonymous request don't call get_user_groups.
        """
        anonymous = auth_models.AnonymousUser()

        request = mock.Mock()
        request.user = anonymous
        request.session = {}
        request.session['user_id'] = 'anonymous'
        request.session['last_updated'] = time.time()
        request.META = {}

        authorization.RequestAuthorization.fill_user_data(request)
        self.assertFalse(get_user_groups.called)

    @mock.patch('datawarehouse.authorization.PolicyAuthorizationBackend.get_user_groups')
    def test_fill_user_data_login(self, get_user_groups):
        """
        Test that fill_user_data updates the information on log in/out.

        When AnonymousUser logs in, user_id is updated and get_user_groups is called.
        """
        request = mock.Mock()
        request.session = {}
        request.META = {}

        user = auth_models.User.objects.create(username='user_foo')

        request.user = user
        request.session['user_id'] = 'anonymous'

        authorization.RequestAuthorization.fill_user_data(request)
        self.assertEqual(user.id, request.session.get('user_id'))
        get_user_groups.assert_called_with(request)

    @mock.patch('datawarehouse.authorization.PolicyAuthorizationBackend.get_user_groups')
    def test_fill_user_data_next_logged_in_request(self, get_user_groups):
        """
        Test that fill_user_data updates the information on log in/out.

        Following logged in request don't call get_user_groups.
        """
        user = auth_models.User.objects.create(username='user_foo')

        request = mock.Mock()
        request.user = user
        request.session = {}
        request.session['user_id'] = user.id
        request.session['last_updated'] = time.time()
        request.META = {}

        authorization.RequestAuthorization.fill_user_data(request)
        self.assertFalse(get_user_groups.called)

    @override_settings(SESSION_AUTH_CACHE_TTL_S=1)
    @mock.patch('datawarehouse.authorization.PolicyAuthorizationBackend.get_user_groups')
    def test_fill_user_data_ttl(self, get_user_groups):
        """
        Test that fill_user_data updates the information when cache expires.

        After SESSION_AUTH_CACHE_TTL_S, the data needs to be set again.
        """
        user = auth_models.User.objects.create(username='user_foo')

        request = mock.Mock()
        request.user = user
        request.session = {}
        request.session['user_id'] = user.id
        request.META = {}

        request.session['last_updated'] = time.time()
        authorization.RequestAuthorization.fill_user_data(request)
        self.assertFalse(get_user_groups.called)

        # Set to less than SESSION_AUTH_CACHE_TTL_S
        request.session['last_updated'] = time.time() - 2
        authorization.RequestAuthorization.fill_user_data(request)
        self.assertTrue(get_user_groups.called)

    @mock.patch('datawarehouse.authorization.PolicyAuthorizationBackend.get_user_groups')
    def test_fill_user_data_logout(self, get_user_groups):
        """
        Test that fill_user_data updates the information on log in/out.

        User logs out and get_user_groups is called.
        """
        request = mock.Mock()
        request.session = {}
        request.META = {}

        anonymous = auth_models.AnonymousUser()
        user = auth_models.User.objects.create(username='user_foo')

        request.user = anonymous
        request.session['user_id'] = user.id

        authorization.RequestAuthorization.fill_user_data(request)
        self.assertEqual('anonymous', request.session.get('user_id'))
        get_user_groups.assert_called_with(request)

    @mock.patch('datawarehouse.authorization.TokenAuthentication.authenticate')
    def test_token_authentication(self, mock_authenticate):
        """
        Test the authentication module tries token authentication.

        As the rest_framework.TokenAuthentication code is executed after
        the middlewares are executed, we need to specifically check if the user
        provided a token and use it to get the user.

        Ensure this happens when calling fill_user_data.
        """
        request = mock.Mock()
        anonymous = auth_models.AnonymousUser()
        request.user = anonymous
        request.session = {}

        request.META = {}
        authorization.RequestAuthorization.fill_user_data(request)
        self.assertFalse(mock_authenticate.called)

        request.META = {'HTTP_AUTHORIZATION': b'foobar'}
        authorization.RequestAuthorization.fill_user_data(request)
        self.assertTrue(mock_authenticate.called)
