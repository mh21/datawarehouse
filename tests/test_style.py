"""Test the styles script."""

from django import test

from datawarehouse import models
from datawarehouse import styles


class StylesTestCase(test.TestCase):
    """Unit tests for the styles script."""

    def test_styles(self):
        """Test that it returns the necessary keys."""
        for level in models.ResultEnum.choices:
            style = styles.get_style(level)
            self.assertIsInstance(style, styles.Style)

    def test_waived(self):
        """Test waived affects the result."""
        self.assertNotEqual(
            styles.get_style(models.ResultEnum.FAIL, waived=False),
            styles.get_style(models.ResultEnum.FAIL, waived=True),
        )

        self.assertNotEqual(
            styles.get_style(models.ResultEnum.ERROR, waived=False),
            styles.get_style(models.ResultEnum.ERROR, waived=True),
        )

    def test_fail_error(self):
        """Test fail and error don't return the same style."""
        self.assertNotEqual(
            styles.get_style(models.ResultEnum.ERROR),
            styles.get_style(models.ResultEnum.FAIL),
        )

        self.assertNotEqual(
            styles.get_style(models.ResultEnum.ERROR, waived=True),
            styles.get_style(models.ResultEnum.FAIL, waived=True),
        )
