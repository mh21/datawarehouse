"""Test the views module."""
import json

from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission
from django.urls import reverse
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

from datawarehouse import models
from datawarehouse import serializers
from tests import utils

User = get_user_model()


class TestAPITokenAuthentication(utils.TestCase):
    # pylint: disable=too-many-instance-attributes, too-many-public-methods
    """Smoke test to keep token auth working."""

    checkout_id = 'a17c8f36b72cc5422a1897ff057eddd2b62ebac2'

    def setUp(self):
        """Set Up."""
        models.KCIDBCheckout.objects.create(
            origin=models.KCIDBOrigin.objects.create(name='redhat'),
            id=self.checkout_id,
            tree=models.GitTree.objects.create(name='test_tree'),
            policy=models.Policy.objects.create(
                name='test_policy',
                read_group=None,
                write_group=None,
            )
        )
        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1")
        models.Issue.objects.create(description='foo bar', ticket_url='http://some.url', kind=issue_kind)

        permissions = Permission.objects.filter(
            codename__in=['add_kcidbcheckout', 'change_kcidbcheckout', 'delete_kcidbcheckout']
        )

        self.test_user = User.objects.create(username='test', email='test@test.com')
        self.test_user.user_permissions.set(permissions)
        token = Token.objects.create(user=self.test_user)

        self.api_client = APIClient()
        self.api_client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

    def test_post(self):
        """Test POST request."""
        issue = models.Issue.objects.first()

        response = self.api_client.post(
            f'/api/1/kcidb/checkouts/{self.checkout_id}/issues',
            json.dumps({'issue_id': issue.id}), content_type="application/json")
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)

    def test_delete(self):
        """Test DELETE request."""
        issue = models.Issue.objects.first()
        models.KCIDBCheckout.objects.get(id=self.checkout_id).issues.add(
            issue
        )

        response = self.api_client.delete(
            f'/api/1/kcidb/checkouts/{self.checkout_id}/issues/{issue.id}',
            content_type="application/json")
        self.assertEqual(status.HTTP_204_NO_CONTENT, response.status_code)

    def test_authorization_middleware(self):
        """
        Test the authorization middleware correctly handles api requests.

        The token authentication is performed after the middleware run, so
        there's a custom piece that handles tokens in the RequestAuthorization
        middleware.
        """
        self.api_client.get('/api/1/kcidb/checkouts')
        self.assertEqual(
            self.api_client.session['user_id'],
            self.test_user.id
        )


class TestTestAPIAnonymous(utils.KCIDBTestCase):
    """Unit tests for the Test API endpoint."""

    fixtures = [
        'tests/fixtures/basic.yaml',
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/issues.yaml',
        'tests/kcidb/fixtures/base_authorization.yaml',
    ]

    anonymous = True
    groups = []

    def test_get_test(self):
        """Test checkouts list endpoint."""
        self._ensure_test_conditions('read')
        authorized_tests = models.Test.objects.filter(
            kcidbtest__build__checkout__id__in=self.checkouts_authorized['read']
        ).distinct()

        response = self.client.get('/api/1/test')
        self.assertEqual(
            serializers.TestSerializer(authorized_tests, many=True).data,
            response.json()['results']
        )

    def test_get_single_test(self):
        """Test checkouts list endpoint."""
        self._ensure_test_conditions('read')
        authorized_tests = models.Test.objects.filter(
            kcidbtest__build__checkout__id__in=self.checkouts_authorized['read']
        )

        for test in models.Test.objects.all():
            response = self.client.get(f'/api/1/test/{test.id}')

            if test not in authorized_tests:
                self.assertEqual(status.HTTP_404_NOT_FOUND, response.status_code)
                continue

            self.assertEqual(
                serializers.TestSerializer(test).data,
                response.json()
            )


class TestTestAPIAnonymousNoGroup(TestTestAPIAnonymous):
    """Unit tests for the Test API endpoint. No group."""

    anonymous = False
    groups = []


class TestTestAPIAnonymousReadGroup(TestTestAPIAnonymous):
    """Unit tests for the Test API endpoint. Read group."""

    anonymous = False
    groups = ['group_a']


class TestTestAPIAnonymousWriteGroup(TestTestAPIAnonymous):
    """Unit tests for the Test API endpoint. Write group."""

    anonymous = False
    groups = ['group_b']


class TestIssueRegexAPIAnonymous(utils.KCIDBTestCase):
    # pylint: disable=too-many-instance-attributes, too-many-public-methods
    """Unit tests for the IssueRegex API endpoint."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/fixtures/issues.yaml',
        'tests/fixtures/issue_regexes.yaml'
    ]

    anonymous = True
    groups = []

    def _ensure_test_conditions(self, method):
        """Check that all conditions for testing are optimal."""
        auth_issue_regexes = models.IssueRegex.objects.filter(issue__id__in=self.issues_authorized[method])
        no_auth_issue_regexes = models.IssueRegex.objects.exclude(issue__id__in=self.issues_authorized[method])

        checks = [
            (auth_issue_regexes, 'No authorized issue_regexes'),
            (no_auth_issue_regexes, 'No unauthorized issue_regexes'),
        ]

        for check, message in checks:
            self.assertTrue(check.exists(), f'Test conditions unmet: {message} for "{method}"')

    def test_list_issue_regex(self):
        """Test get issue regex list."""
        self._ensure_test_conditions('read')
        authorized_issue_regexes = models.IssueRegex.objects.filter(
            issue__id__in=self.issues_authorized['read']
        )

        response = self.client.get('/api/1/issue/-/regex')

        self.assertEqual(
            response.json()['results'],
            serializers.IssueRegexSerializer(authorized_issue_regexes, many=True).data
        )

    def test_get_issue_regex(self):
        """Test get issue regex."""
        self._ensure_test_conditions('read')
        authorized_issue_regexes = models.IssueRegex.objects.filter(
            issue__id__in=self.issues_authorized['read']
        )
        for issue_regex in models.IssueRegex.objects.all():
            response = self.client.get(f'/api/1/issue/-/regex/{issue_regex.id}')

            if issue_regex not in authorized_issue_regexes:
                self.assertEqual(status.HTTP_404_NOT_FOUND, response.status_code)
                continue

            self.assertEqual(
                response.json(),
                serializers.IssueRegexSerializer(issue_regex).data,
            )


class TestIssueRegexAPINoGroup(TestIssueRegexAPIAnonymous):
    """Unit tests for the IssueRegex API endpoint. No group."""

    anonymous = False
    groups = []


class TestIssueRegexAPIReadGroup(TestIssueRegexAPIAnonymous):
    """Unit tests for the IssueRegex API endpoint. Read group."""

    anonymous = False
    groups = ['group_a']


class TestIssueRegexAPIWriteGroup(TestIssueRegexAPIAnonymous):
    """Unit tests for the IssueRegex API endpoint. Write group."""

    anonymous = False
    groups = ['group_b']


class TestIssueRegexAPIAllGroups(TestIssueRegexAPIAnonymous):
    """Unit tests for the IssueRegex API endpoint. All groups."""

    anonymous = False
    groups = ['group_a', 'group_b']


class TestIssueAPIAnonymous(utils.KCIDBTestCase):
    """Tests for the Issue endpoints."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/fixtures/issues.yaml',
    ]
    anonymous = True
    groups = []

    def _ensure_test_conditions(self, method):
        """Check that all conditions for testing are optimal."""
        auth_issues = models.Issue.objects.filter(id__in=self.issues_authorized[method])
        no_auth_issues = models.Issue.objects.exclude(id__in=self.issues_authorized[method])

        checks = [
            (auth_issues, 'No authorized issues'),
            (no_auth_issues, 'No unauthorized issues'),
        ]

        for check, message in checks:
            self.assertTrue(check.exists(), f'Test conditions unmet: {message} for "{method}"')

    def test_list_issues(self):
        """Test list issues."""
        self._ensure_test_conditions('read')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['read']
        )

        response = self.client.get('/api/1/issue/')

        self.assertEqual(
            response.json()['results'],
            serializers.IssueSerializer(authorized_issues, many=True).data,
        )

    def test_list_issues_resolved(self):
        """Test list resolved issues."""
        self._ensure_test_conditions('read')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['read']
        )

        response = self.client.get('/api/1/issue/?resolved=True')

        self.assertEqual(
            response.json()['results'],
            serializers.IssueSerializer(authorized_issues.exclude(resolved_at=None), many=True).data,
        )

    def test_get_issue(self):
        """Test get issue."""
        self._ensure_test_conditions('read')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['read']
        )

        for issue in models.Issue.objects.all():
            response = self.client.get(f'/api/1/issue/{issue.id}/')

            if issue not in authorized_issues:
                self.assertEqual(status.HTTP_404_NOT_FOUND, response.status_code)
                continue

            self.assertEqual(
                response.json(),
                serializers.IssueSerializer(issue).data,
            )

    def test_update_issue(self):
        """Test update of issue."""
        self._ensure_test_conditions('write')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['write']
        )

        for issue in models.Issue.objects.all():
            prev_description = issue.description
            edited_description = prev_description + " Edit"
            patch_issue_url = reverse('issue-detail', args=[issue.id])

            response = self.client.patch(
                patch_issue_url,
                data={'description': edited_description},
                content_type='application/json'
            )

            issue.refresh_from_db()
            if issue not in authorized_issues:
                self.assertEqual(status.HTTP_404_NOT_FOUND, response.status_code)
                self.assertEqual(
                    prev_description,
                    issue.description,
                )

            else:
                self.assertEqual(status.HTTP_200_OK, response.status_code)
                self.assertEqual(
                    edited_description, issue.description,
                )

    def test_delete_issue(self):
        """Test delete of issue."""
        self._ensure_test_conditions('write')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['write']
        )

        for issue in models.Issue.objects.all():
            delete_issue_url = reverse('issue-detail', args=[issue.id])

            response = self.client.delete(
                delete_issue_url,
                content_type='application/json'
            )

            if issue not in authorized_issues:
                self.assertEqual(status.HTTP_404_NOT_FOUND, response.status_code)
                self.assertIn(issue, models.Issue.objects.all())
            else:
                self.assertEqual(status.HTTP_204_NO_CONTENT, response.status_code)
                self.assertNotIn(issue, models.Issue.objects.all())


class TestIssueAPINoGroups(TestIssueAPIAnonymous):
    """Tests for the Issue endpoints with no groups."""

    anonymous = False
    groups = []


class TestIssueAPIReadGroup(TestIssueAPIAnonymous):
    """Tests for the Issue endpoints with read groups."""

    anonymous = False
    groups = ['group_a']


class TestIssueAPIWriteGroup(TestIssueAPIAnonymous):
    """Tests for the Issue endpoints with write groups."""

    anonymous = False
    groups = ['group_b']


class TestIssueAPIAllGroups(TestIssueAPIAnonymous):
    """Tests for the Issue endpoints with read and write groups."""

    anonymous = False
    groups = ['group_a', 'group_b']


class TestIssueAPICreation(utils.TestCase):

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/fixtures/issues.yaml',
        'tests/fixtures/issue_regexes.yaml'
    ]

    def _setup_test_data(self, data_overlay=None):
        data = {
            'kind': 1,
            'description': 'Issue Public New',
            'ticket_url': 'https://issue.public.new',
            'policy': 1,
            'created_at': '2021-01-01T01:10:20.000Z',
            'last_edited_at': '2021-01-01T01:10:20.000Z'
        }

        if data_overlay:
            data.update(data_overlay)
        return data

    def test_create_issue(self):
        """Test create of issue."""
        create_issue_url = reverse('issue-list')

        cases = [
            ('Forbidden user', {'description': 'Issue forbidden user'}, False),
            ('Authenticated user', {'description': 'Issue authenticated user'}, True),
            ('Authenticated user new',
             {'description': 'Issue public new 2 auth', 'ticket_url': 'https://issue.public.new.auth'},
             True),
        ]

        for name, data, allowed in cases:
            with self.subTest(name):
                post_data = self._setup_test_data(data)

                if allowed:
                    self.assert_authenticated_post(
                        status.HTTP_201_CREATED,
                        'add_issue',
                        create_issue_url,
                        post_data,
                        content_type='application/json')

                    new_issue = models.Issue.objects.get(description=post_data['description'])
                    self.assertEqual(new_issue.ticket_url, post_data['ticket_url'])
                else:
                    self.assert_authenticated_anonymous_forbidden(
                        'post',
                        create_issue_url,
                        post_data,
                        content_type='application/json'
                    )

                    self.assertRaises(
                        models.Issue.DoesNotExist,
                        models.Issue.objects.get,
                        description=post_data['description']
                    )

    def test_create_invalid_issue(self):
        """Test create with invalid data."""
        create_issue_url = reverse('issue-list')

        cases = [
            ('Invalid data', {'kind': {}, 'description': 'Issue invalid data'}),
            ('Invalid url', {'description': 'Issue invalid data', 'ticket_url': 'https://issue.public'}),
        ]

        for name, data in cases:
            with self.subTest(name):
                post_data = self._setup_test_data(data)
                self.assert_authenticated_post(
                    status.HTTP_400_BAD_REQUEST,
                    'add_issue',
                    create_issue_url,
                    post_data,
                    content_type='application/json'
                )

                self.assertRaises(
                    models.Issue.DoesNotExist,
                    models.Issue.objects.get,
                    description=post_data['description']
                )
