"""Test cron.py."""
import datetime
from unittest import mock

from django.contrib.auth import get_user_model
from django.test.utils import override_settings
from django.utils import timezone
from freezegun import freeze_time

from datawarehouse import cron
from datawarehouse import models
from tests import utils


class TestCron(utils.TestCase):
    """Test cron __init__.py."""

    def test_jobs(self):
        """Ensure that the list of jobs contains all the expected jobs."""
        expected_jobs = {
            cron.BuildSetupsFinishedCheckouts,
            cron.ClearExpiredSessions,
            cron.DeleteExpiredArtifacts,
            cron.DeleteOldNotConfirmedUsers,
            cron.ReadyToReportCheckouts,
            cron.RemoveExpiredCaptachas,
            cron.RunQueuedTasks,
            cron.SendAccountDeletionWarning,
            cron.SendQueuedEmails,
            cron.SendQueuedMessages,
            cron.TestsFinishedCheckouts,
            cron.UpdateLdapGroupMembers,
            cron.UpdatePrometheusMetrics5m,
            cron.UpdatePrometheusMetrics1h,
        }
        self.assertEqual(len(expected_jobs), len(cron.JOBS))
        self.assertEqual(
            set(job.__class__ for job in cron.JOBS),
            expected_jobs
        )

    @mock.patch('django.db.connection.close')
    def test_jobs_close_connections(self, mock_conn_close):
        """Ensure that all cron jobs close DB connection after running."""
        for job in cron.JOBS:
            job.entrypoint = mock.Mock()
            job.run()
            self.assertTrue(job.entrypoint.called)
            self.assertTrue(mock_conn_close.called)
            mock_conn_close.reset_mock()


class TestDeleteArtifacts(utils.TestCase):
    """Test cron.delete_expired_artifacts method."""

    @staticmethod
    def _expiry_in(valid_for):
        """Calculate expiry date."""
        return timezone.now() + datetime.timedelta(days=valid_for)

    def test_delete(self):
        """Test delete_expired_artifacts deletes expired artifacts."""
        models.Artifact.objects.bulk_create([
            # Expired
            models.Artifact(name='test_1', url='http://test_1', valid_for=1, expiry_date=self._expiry_in(-2)),
            models.Artifact(name='test_2', url='http://test_2', valid_for=1, expiry_date=self._expiry_in(-1)),
            # Still valid
            models.Artifact(name='test_3', url='http://test_3', valid_for=1, expiry_date=self._expiry_in(1)),
            models.Artifact(name='test_4', url='http://test_4', valid_for=1, expiry_date=self._expiry_in(2)),
        ])

        self.assertEqual(4, models.Artifact.objects.count())
        cron.DeleteExpiredArtifacts().entrypoint()
        self.assertEqual(2, models.Artifact.objects.count())

        # These would raise if not present.
        models.Artifact.objects.get(name='test_3')
        models.Artifact.objects.get(name='test_4')


class TestDeleteOldNotConfirmedUsers(utils.TestCase):
    """Test cron.delete_old_not_confirmed_users method"""

    @staticmethod
    def _shifted_days(days):
        """Calculate shifted date."""
        return timezone.now() + datetime.timedelta(days=days)

    @override_settings(FF_DEL_NOT_CONFIRMED_USERS_OLDER_THAN_DAYS=30)
    def test_delete(self):
        """Test if old invalid users get deleted"""
        User = get_user_model()

        # Create test users
        User.objects.bulk_create([
            User(username="old_test_1", date_joined=self._shifted_days(-32)),
            User(username="old_test_2", date_joined=self._shifted_days(-32), last_login=self._shifted_days(-2)),
            User(username="old_test_3", date_joined=self._shifted_days(-15)),
            User(username="old_test_4", date_joined=self._shifted_days(-15), last_login=self._shifted_days(-2)),
        ])

        self.assertEqual(4, User.objects.count())
        cron.DeleteOldNotConfirmedUsers().entrypoint()
        self.assertEqual(3, User.objects.count())

        User.objects.get(username="old_test_2")
        User.objects.get(username="old_test_3")
        User.objects.get(username="old_test_4")

    @override_settings(FF_DEL_NOT_CONFIRMED_USERS_OLDER_THAN_DAYS=None)
    def test_delete_disabled(self):
        """Test if old invalid users get deleted"""
        User = get_user_model()

        # Create test users
        User.objects.bulk_create([
            User(username="old_test_1", date_joined=self._shifted_days(-32)),
            User(username="old_test_2", date_joined=self._shifted_days(-32), last_login=self._shifted_days(-2)),
            User(username="old_test_3", date_joined=self._shifted_days(-15)),
            User(username="old_test_4", date_joined=self._shifted_days(-15), last_login=self._shifted_days(-2)),
        ])

        self.assertEqual(4, User.objects.count())
        cron.DeleteOldNotConfirmedUsers().entrypoint()
        self.assertEqual(4, User.objects.count())

    @override_settings(FF_DEL_NOT_CONFIRMED_USERS_OLDER_THAN_DAYS=30)
    @mock.patch('datawarehouse.utils.EMAIL_QUEUE.add')
    def test_send_deletion_notification(self, add_mock):
        """Test if email gets sent."""
        User = get_user_model()

        # Create test users
        User.objects.bulk_create([
            User(username="del_test_1", email="del1@mail.com",
                 date_joined=self._shifted_days(-32)),
            User(username="del_test_2", email="del2@mail.com",
                 date_joined=self._shifted_days(-32), last_login=self._shifted_days(-2)),
            User(username="del_test_3", email="del3@mail.com",
                 date_joined=self._shifted_days(-15)),
            User(username="del_test_4", email="del4@mail.com",
                 date_joined=self._shifted_days(-15), last_login=self._shifted_days(-2)),
        ])

        self.assertEqual(4, User.objects.count())
        cron.SendAccountDeletionWarning().entrypoint()

        # Check if mails have been sent to correct recipients
        self.assertEqual(2, add_mock.call_count)
        calls = [
            mock.call(mock.ANY, mock.ANY, "del1@mail.com"),
            mock.call(mock.ANY, mock.ANY, "del3@mail.com")
        ]
        add_mock.assert_has_calls(calls, any_order=True)

        # Check if we don't send multiple emails
        cron.SendAccountDeletionWarning().entrypoint()
        self.assertEqual(2, add_mock.call_count)

    @override_settings(FF_DEL_NOT_CONFIRMED_USERS_OLDER_THAN_DAYS=0)
    @mock.patch('datawarehouse.utils.EMAIL_QUEUE.add')
    def test_send_deletion_notification_disabled(self, add_mock):
        """Test if email doesn't get sent."""
        User = get_user_model()

        # Create test users
        User.objects.bulk_create([
            User(username="del_test_1", email="del1@mail.com",
                 date_joined=self._shifted_days(-32)),
            User(username="del_test_2", email="del2@mail.com",
                 date_joined=self._shifted_days(-32), last_login=self._shifted_days(-2)),
            User(username="del_test_3", email="del3@mail.com",
                 date_joined=self._shifted_days(-15)),
            User(username="del_test_4", email="del4@mail.com",
                 date_joined=self._shifted_days(-15), last_login=self._shifted_days(-2)),
        ])

        self.assertEqual(4, User.objects.count())
        cron.SendAccountDeletionWarning().entrypoint()
        self.assertEqual(0, add_mock.call_count)


class TestReadyToReportCheckouts(utils.TestCase):
    """Test cron.ReadyToReportCheckouts."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
    ]

    @mock.patch('datawarehouse.metrics.update_checkout_metrics')
    @mock.patch('datawarehouse.metrics.update_time_to_report')
    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_run(self, mock_send, mock_update_time_to_report, mock_update_checkout_metrics):
        """Check messages are sent and the objects tagged as ready_to_report."""
        models.KCIDBCheckout.objects.update(valid=True, last_triaged_at=timezone.now())
        models.KCIDBBuild.objects.update(valid=True, last_triaged_at=timezone.now())
        models.KCIDBTest.objects.update(status='P', last_triaged_at=timezone.now())

        self.assertEqual(3, models.KCIDBCheckout.objects.filter_ready_to_report().count())
        cron.ReadyToReportCheckouts().entrypoint()
        self.assertEqual(0, models.KCIDBCheckout.objects.filter_ready_to_report().count())

        mock_send.assert_has_calls([
            mock.call.send(
                sender='cron.jobs.ReadyToReportCheckouts',
                status=models.ObjectStatusEnum.READY_TO_REPORT,
                object_type='checkout',
                objects=list(models.KCIDBCheckout.objects.all()),
            ),
        ])

        mock_update_time_to_report.assert_has_calls(
            [mock.call(checkout) for checkout in models.KCIDBCheckout.objects.all()]
        )
        mock_update_checkout_metrics.assert_has_calls(
            [mock.call(checkout) for checkout in models.KCIDBCheckout.objects.all()]
        )


class TestBuildSetupsFinishedCheckouts(utils.TestCase):
    """Test cron.BuildSetupsFinishedCheckouts."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
    ]

    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_run(self, signal):
        """Check messages are sent when all the build setups finished."""
        models.KCIDBCheckout.objects.update(notification_sent_build_setups_finished=False)
        models.KCIDBBuild.objects.update(kpet_tree_name='kpet_tree_name')

        self.assertEqual(1, models.KCIDBCheckout.objects.filter_build_setups_finished().count())
        cron.BuildSetupsFinishedCheckouts().entrypoint()
        self.assertEqual(0, models.KCIDBCheckout.objects.filter_build_setups_finished().count())

        signal.send.assert_has_calls([
            mock.call(
                sender='cron.jobs.BuildSetupsFinishedCheckouts',
                status=models.ObjectStatusEnum.BUILD_SETUPS_FINISHED,
                object_type='checkout',
                objects=[checkout]
            )
            for checkout in models.KCIDBCheckout.objects.filter_has_builds()
        ])

        self.assertEqual(models.KCIDBCheckout.objects.filter_has_builds().count(),
                         signal.send.call_count)


class TestTestsFinishedCheckouts(utils.TestCase):
    """Test cron.TestsFinishedCheckouts."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
    ]

    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_run(self, signal):
        """Check messages are sent when all the tests finished."""
        models.KCIDBCheckout.objects.update(notification_sent_build_setups_finished=False)
        models.KCIDBTest.objects.update(status='P')

        self.assertEqual(1, models.KCIDBCheckout.objects.filter_tests_finished().count())
        cron.TestsFinishedCheckouts().entrypoint()
        self.assertEqual(0, models.KCIDBCheckout.objects.filter_tests_finished().count())

        signal.send.assert_has_calls([
            mock.call(
                sender='cron.jobs.TestsFinishedCheckouts',
                status=models.ObjectStatusEnum.TESTS_FINISHED,
                object_type='checkout',
                objects=[checkout]
            )
            for checkout in models.KCIDBCheckout.objects.filter_has_tests()
        ])

        self.assertEqual(models.KCIDBCheckout.objects.filter_has_tests().count(),
                         signal.send.call_count)


class TestUpdatePrometheusMetrics5m(utils.TestCase):
    """Test cron.UpdatePrometheusMetrics5m."""

    @mock.patch('datawarehouse.metrics.update_unfinished_builds')
    @mock.patch('datawarehouse.metrics.update_unfinished_tests')
    def test_run(self, mock_update_unfinished_tests, mock_update_unfinished_builds):
        """Check metric update functions are called."""
        cron.UpdatePrometheusMetrics5m().entrypoint()
        self.assertTrue(mock_update_unfinished_builds.called)
        self.assertTrue(mock_update_unfinished_tests.called)


class TestUpdatePrometheusMetrics1h(utils.TestCase):
    """Test cron.UpdatePrometheusMetrics1h."""

    @mock.patch('datawarehouse.metrics.update_issues')
    def test_run(self, mock_update_issues):
        """Check metric update functions are called."""
        cron.UpdatePrometheusMetrics1h().entrypoint()
        self.assertTrue(mock_update_issues.called)


class TestSendQueuedMessages(utils.TestCase):
    """Test cron.SendQueuedMessages."""

    @override_settings(RABBITMQ_SEND_ENABLED=True)
    def test_run(self):
        """Check send messages function is called."""
        cron.jobs.utils.MSG_QUEUE = mock.Mock()
        cron.SendQueuedMessages().entrypoint()
        self.assertTrue(cron.jobs.utils.MSG_QUEUE.send.called)


class TestSendQueuedEmails(utils.TestCase):
    """Test cron.SendQueuedEmails."""

    @override_settings(EMAIL_SEND_ENABLED=True)
    def test_run(self):
        """Check send messages function is called."""
        cron.jobs.utils.EMAIL_QUEUE = mock.Mock()
        cron.SendQueuedEmails().entrypoint()
        self.assertTrue(cron.jobs.utils.EMAIL_QUEUE.send.called)

    @override_settings(EMAIL_SEND_ENABLED=False)
    def test_run_disabled(self):
        """Check send messages function is not called."""
        cron.jobs.utils.EMAIL_QUEUE = mock.Mock()
        cron.SendQueuedEmails().entrypoint()
        self.assertFalse(cron.jobs.utils.EMAIL_QUEUE.send.called)


class TestRunQueuedTasks(utils.TestCase):
    """Test cron.RunQueuedTasks."""

    @mock.patch('datawarehouse.models.task_models.celery.app.signature')
    def test_run(self, mock_celery):
        """Check QueuedTasks are run."""
        function_task_1 = mock.Mock()

        def mocked_signature(name):
            return {'task_1': function_task_1}[name]

        mock_celery.side_effect = mocked_signature

        with freeze_time("2010-01-02 09:00:00"):
            models.QueuedTask.create(
                name='task_1',
                call_id='task_1_1',
                call_kwargs={'foo': 'bar'}
            )

        # Too early, shouldn't run task_1
        with freeze_time("2010-01-02 09:01:00"):
            cron.RunQueuedTasks().entrypoint()

        self.assertFalse(function_task_1.called)

        # Enought time passed, should run task_1
        with freeze_time("2010-01-02 09:10:00"):
            cron.RunQueuedTasks().entrypoint()

        self.assertTrue(function_task_1.called)
        function_task_1.assert_called_with([{'foo': 'bar'}])

    @mock.patch('datawarehouse.models.task_models.celery.app.signature')
    def test_run_failure(self, mock_celery):
        """Check QueuedTasks behaviour on failure."""
        function_task_1 = mock.Mock(side_effect=Exception)
        function_task_2 = mock.Mock()

        def mocked_signature(name):
            return {
                'task_1': function_task_1,
                'task_2': function_task_2,
            }[name]

        mock_celery.side_effect = mocked_signature

        with freeze_time("2010-01-02 09:00:00"):
            models.QueuedTask.create(
                name='task_1',
                call_id='task_1',
                call_kwargs={'foo': 'bar'}
            )
            models.QueuedTask.create(
                name='task_2',
                call_id='task_2',
                call_kwargs={'foo': 'bar'}
            )

        with freeze_time("2010-01-02 09:10:00"):
            cron.RunQueuedTasks().entrypoint()

        self.assertTrue(function_task_1.called)
        self.assertTrue(function_task_2.called)

        # Task 1 failed so it still exists, will be retried next iteration.
        self.assertTrue(
            models.QueuedTask.objects.filter(name='task_1', call_id='task_1').exists()
        )

        # Task 2 run successfully even after Task 1 failed
        self.assertFalse(
            models.QueuedTask.objects.filter(name='task_2', call_id='task_2').exists()
        )
