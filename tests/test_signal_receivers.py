"""Test the signal_receivers module."""
from unittest import mock

from django import test
from django.contrib.auth import get_user_model
from freezegun import freeze_time

from datawarehouse import models
from datawarehouse import signal_receivers
from datawarehouse import signals
from datawarehouse.api.kcidb import serializers as kcidb_serializers


class SignalsTest(test.TestCase):
    """Unit tests for the Signals module."""

    @staticmethod
    @freeze_time("2010-01-02 09:00:00")
    @mock.patch('datawarehouse.signal_receivers.utils.send_kcidb_notification')
    def test_send_kcidb_message(send_kcidb):
        """Test that the kcidb message is sent correctly."""
        rev = models.KCIDBCheckout.objects.create(
            id='https://git.kernel.org/linux.git@decd6167bf4f6bec1284006d0522381b44660df3',
            origin=models.KCIDBOrigin.objects.create(name='redhat'),
        )
        signal_receivers.send_kcidb_object_message(
            status='some-status',
            object_type='checkout',
            objects=[rev],
        )
        send_kcidb.assert_called_with([{
            'timestamp': '2010-01-02T09:00:00+00:00',
            'status': 'some-status',
            'object_type': 'checkout',
            'object': kcidb_serializers.KCIDBCheckoutSerializer(rev).data,
            'id': rev.id,
            'iid': rev.iid,
            'misc': {},
        }])

    @mock.patch('datawarehouse.signal_receivers.utils.send_kcidb_notification', mock.Mock())
    def test_signal_kcidb_object(self):
        """Test kcidb_object signal receivers."""
        rev = models.KCIDBCheckout.objects.create(
            id='https://git.kernel.org/linux.git@decd6167bf4f6bec1284006d0522381b44660df3',
            origin=models.KCIDBOrigin.objects.create(name='redhat'),
        )
        receivers = signals.kcidb_object.send(
            sender='test', status='test', object_type='checkout', objects=[rev]
        )
        self.assertEqual(
            [(signal_receivers.send_kcidb_object_message, None)],
            receivers
        )


class IssueOccurrenceAssigned(test.TestCase):
    """
    Test issue_occurrence_assigned receiver.

    Ensure necessary functions are called.
    """

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
        'tests/kcidb/fixtures/issues.yaml',
    ]

    def setUp(self):
        """Set up test."""
        self.issue = models.Issue.objects.last()

    @mock.patch('datawarehouse.signal_receivers.scripts.send_regression_notification')
    @mock.patch('datawarehouse.signal_receivers.scripts.update_issue_occurrences_regression')
    @mock.patch('datawarehouse.signal_receivers.scripts.update_issue_policy')
    @mock.patch('datawarehouse.signal_receivers.scripts.update_issue_occurrences_related_checkout')
    def test_checkout(self, update_related_checkout, update_issue_policy, update_regression, send_notif):
        """"Test issue_occurrence_assigned receiver with checkout."""
        checkout = models.KCIDBCheckout.objects.last()

        checkout.issues.add(self.issue)
        update_related_checkout.assert_called_with(checkout, {self.issue.id})
        update_issue_policy.assert_called_with({self.issue.id})
        update_regression.assert_called_with(checkout, {self.issue.id})
        send_notif.assert_called_with(checkout, {self.issue.id})

    @mock.patch('datawarehouse.signal_receivers.scripts.send_regression_notification')
    @mock.patch('datawarehouse.signal_receivers.scripts.update_issue_occurrences_regression')
    @mock.patch('datawarehouse.signal_receivers.scripts.update_issue_policy')
    @mock.patch('datawarehouse.signal_receivers.scripts.update_issue_occurrences_related_checkout')
    def test_build(self, update_related_checkout, update_issue_policy, update_regression, send_notif):
        """"Test issue_occurrence_assigned receiver with build."""
        build = models.KCIDBBuild.objects.last()

        build.issues.add(self.issue)
        update_related_checkout.assert_called_with(build, {self.issue.id})
        update_issue_policy.assert_called_with({self.issue.id})
        update_regression.assert_called_with(build, {self.issue.id})
        send_notif.assert_called_with(build, {self.issue.id})

    @mock.patch('datawarehouse.signal_receivers.scripts.send_regression_notification')
    @mock.patch('datawarehouse.signal_receivers.scripts.update_issue_occurrences_regression')
    @mock.patch('datawarehouse.signal_receivers.scripts.update_issue_policy')
    @mock.patch('datawarehouse.signal_receivers.scripts.update_issue_occurrences_related_checkout')
    def test_test(self, update_related_checkout, update_issue_policy, update_regression, send_notif):
        """"Test issue_occurrence_assigned receiver with test."""
        test = models.KCIDBTest.objects.last()

        test.issues.add(self.issue)
        update_related_checkout.assert_called_with(test, {self.issue.id})
        update_issue_policy.assert_called_with({self.issue.id})
        update_regression.assert_called_with(test, {self.issue.id})
        send_notif.assert_called_with(test, {self.issue.id})

    @mock.patch('datawarehouse.signal_receivers.scripts.send_regression_notification')
    @mock.patch('datawarehouse.signal_receivers.scripts.update_issue_occurrences_regression')
    @mock.patch('datawarehouse.signal_receivers.scripts.update_issue_policy')
    @mock.patch('datawarehouse.signal_receivers.scripts.update_issue_occurrences_related_checkout')
    def test_multiple_add(self, update_related_checkout, update_issue_policy, update_regression, send_notif):
        """"Test issue_occurrence_assigned receiver when adding multiple objects."""
        test = models.KCIDBTest.objects.last()
        issues = models.Issue.objects.all()

        test.issues.add(*issues)
        update_related_checkout.assert_called_with(test, {i.id for i in issues})
        update_issue_policy.assert_called_with({i.id for i in issues})
        update_regression.assert_called_with(test, {i.id for i in issues})
        send_notif.assert_called_with(test, {i.id for i in issues})

    @mock.patch('datawarehouse.signal_receivers.scripts.send_regression_notification')
    @mock.patch('datawarehouse.signal_receivers.scripts.update_issue_occurrences_regression')
    @mock.patch('datawarehouse.signal_receivers.scripts.update_issue_policy')
    @mock.patch('datawarehouse.signal_receivers.scripts.update_issue_occurrences_related_checkout')
    def test_multiple_set(self, update_related_checkout, update_issue_policy, update_regression, send_notif):
        """"Test issue_occurrence_assigned receiver when setting multiple objects."""
        test = models.KCIDBTest.objects.last()
        issues = models.Issue.objects.all()

        test.issues.set(issues)
        update_related_checkout.assert_called_with(test, {i.id for i in issues})
        update_issue_policy.assert_called_with({i.id for i in issues})
        update_regression.assert_called_with(test, {i.id for i in issues})
        send_notif.assert_called_with(test, {i.id for i in issues})

    @mock.patch('datawarehouse.signal_receivers.scripts.send_regression_notification')
    @mock.patch('datawarehouse.signal_receivers.scripts.update_issue_occurrences_regression')
    @mock.patch('datawarehouse.signal_receivers.scripts.update_issue_policy')
    @mock.patch('datawarehouse.signal_receivers.scripts.update_issue_occurrences_related_checkout')
    def test_existing(self, update_related_checkout, update_issue_policy, update_regression, send_notif):
        """"
        Test issue_occurrence_assigned receiver when adding already added objects.

        Adding the objects for a second time should call the receiver without those ids,
        making it safe to asume the functions are called only the first time.
        """
        test = models.KCIDBTest.objects.last()
        issues = models.Issue.objects.all()

        test.issues.add(*issues)
        update_related_checkout.assert_called_with(test, {i.id for i in issues})
        update_issue_policy.assert_called_with({i.id for i in issues})
        update_regression.assert_called_with(test, {i.id for i in issues})
        send_notif.assert_called_with(test, {i.id for i in issues})

        test.issues.add(*issues)
        update_related_checkout.assert_called_with(test, set())
        update_issue_policy.assert_called_with(set())
        update_regression.assert_called_with(test, set())
        send_notif.assert_called_with(test, set())

        test.issues.set(issues)
        update_related_checkout.assert_called_with(test, set())
        update_issue_policy.assert_called_with(set())
        update_regression.assert_called_with(test, set())
        send_notif.assert_called_with(test, set())


class TestSyncUserLdapGroups(test.TestCase):
    """Test sync_user_ldap_groups receiver."""

    @mock.patch('datawarehouse.signal_receivers.scripts.update_ldap_group_members_for_user')
    def test_call_created_true(self, update_ldap_group_members_for_user):
        """Test update_ldap_group_members_for_user is called on user creation."""
        signal_receivers.sync_user_ldap_groups(created=True, instance=mock.Mock())
        self.assertTrue(update_ldap_group_members_for_user.called)

    @mock.patch('datawarehouse.signal_receivers.scripts.update_ldap_group_members_for_user')
    def test_call_created_false(self, update_ldap_group_members_for_user):
        """Test update_ldap_group_members_for_user is not called on user modification."""
        signal_receivers.sync_user_ldap_groups(created=False, instance=mock.Mock())
        self.assertFalse(update_ldap_group_members_for_user.called)

    @mock.patch('datawarehouse.signal_receivers.scripts.update_ldap_group_members_for_user')
    def test_signals(self, update_ldap_group_members_for_user):
        """Test sync_user_ldap_groups is correctly called on signals."""
        User = get_user_model()

        # Creation
        user = User.objects.create(username='test')
        self.assertTrue(update_ldap_group_members_for_user.called)

        update_ldap_group_members_for_user.reset_mock()

        # Update
        user.save()
        self.assertFalse(update_ldap_group_members_for_user.called)

        update_ldap_group_members_for_user.reset_mock()

        # Deletion
        user.delete()
        self.assertFalse(update_ldap_group_members_for_user.called)
