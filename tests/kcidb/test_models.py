# pylint: disable=too-many-lines
"""Test model creation from KCIDB data."""
import datetime
import json
import os
import pathlib
from unittest import mock

from django.db.models.query import QuerySet
from django.utils import timezone
import responses

from datawarehouse import models
from tests import utils


def load_json(name):
    """Load json file from assets."""
    file_name = os.path.join(utils.ASSETS_DIR, name)
    file_content = pathlib.Path(file_name).read_text()

    return json.loads(file_content)


def mock_patch():
    """Mock patch requests."""
    patch_body = """MIME-Version: 1.0
Subject: [RHEL PATCH 206/206] fix some stuff, and break some other
commit 56887cffe946bb0a90c74429fa94d6110a73119d
Author: Patch Author <patch@author.com>
Date:   Mon Feb 22 10:48:09 2021 +0100

    fix some stuff, and break some other
    """
    responses.add(responses.GET, 'http://patchwork.server/patch/2322797/mbox/', body=patch_body)


@mock.patch('datawarehouse.signals.kcidb_object.send', mock.Mock())
class TestKCIDBCheckoutFromJson(utils.TestCase):
    """Test creation of KCIDBCheckout model."""

    def test_basic(self):
        """Submit only id."""
        data = {
            'id': 'redhat:1',
            'origin': 'redhat',
        }
        checkout = models.KCIDBCheckout.create_from_json(data)
        self.assertIsInstance(checkout, models.KCIDBCheckout)

    def test_re_submit(self):
        """Submit twice. Updates current data."""
        data = {
            'id': 'redhat:1',
            'origin': 'redhat',
        }
        checkout = models.KCIDBCheckout.create_from_json(data)
        self.assertIsNone(checkout.message_id)

        # Re submit with new data.
        data['message_id'] = '<foo@bar.com>'
        checkout = models.KCIDBCheckout.create_from_json(data)
        self.assertEqual(checkout.message_id, data['message_id'])

        # Re submit with new data, without the previous value.
        del data['message_id']
        data['valid'] = True
        checkout = models.KCIDBCheckout.create_from_json(data)
        self.assertIsNotNone(checkout.message_id)
        self.assertEqual(checkout.valid, data['valid'])

        # Check there's only one instance
        self.assertEqual(
            1,
            models.KCIDBCheckout.objects.filter(id=data['id']).count()
        )

    @responses.activate
    def test_patch(self):
        """Submit email patches."""
        body = (
            b'From foo@baz Mon 25 Nov 2019 02:27:19 PM CET\n'
            b'From: Some One <some-one@redhat.com>\n'
            b'Date: Tue, 19 Nov 2019 23:47:33 +0100\n'
            b'Subject: fix something somewhere\n'
            b'..')
        responses.add(responses.GET, 'http://some-patch.server/patch/1234', body=body)
        responses.add(responses.GET, 'http://some-patch.server/patch/1235', body=body)
        data = {
            'id': 'redhat:1',
            'origin': 'redhat',
            'patchset_files': [
                {'url': 'http://some-patch.server/patch/1234', 'name': '1234'},
                {'url': 'http://some-patch.server/patch/1235'}
            ],
        }

        checkout = models.KCIDBCheckout.create_from_json(data)
        self.assertEqual(2, checkout.patches.count())
        self.assertFalse(hasattr(checkout.patches.first(), 'patchworkpatch'))

        expected_keys = [
            # If "name" is provided, use it as a subject
            ('http://some-patch.server/patch/1234', '1234'),
            # Otherwise, use the subject from the patch body
            ('http://some-patch.server/patch/1235', 'fix something somewhere'),
        ]
        patch_keys = [patch.natural_key() for patch in checkout.patches.all()]
        self.assertEqual(expected_keys, patch_keys)

    def test_log_url(self):
        """Submit log_url."""
        data = {
            'id': 'redhat:1',
            'origin': 'redhat',
            'log_url': 'http://log.server/log.name',
        }

        checkout = models.KCIDBCheckout.create_from_json(data)

        self.assertIsInstance(checkout.log, models.Artifact)
        self.assertEqual('http://log.server/log.name', checkout.log.url)
        self.assertEqual('log.name', checkout.log.name)

    def test_contacts(self):
        """Test contact submission."""
        data = {
            'id': 'redhat:1',
            'origin': 'redhat',
            'contacts': ['someone@email.com', 'Some Other <some-other@mail.com>']
        }

        checkout = models.KCIDBCheckout.create_from_json(data)

        self.assertEqual(2, checkout.contacts.count())
        contact_1 = checkout.contacts.get(email='someone@email.com')
        self.assertEqual('', contact_1.name)
        contact_2 = checkout.contacts.get(email='some-other@mail.com')
        self.assertEqual('Some Other', contact_2.name)

    @responses.activate
    def test_all_data(self):
        """Check it creates all the objects."""
        mock_patch()
        data = {
            'id': 'redhat:1',
            'origin': 'redhat',
            'tree_name': 'arm',
            'git_repository_url': 'https://repository.com/repo/kernel-rhel',
            'git_repository_branch': 'rhel-1.2.3',
            'git_commit_hash': '403cbf29a4e277ad4872515ec3854b175960bbdf',
            'git_commit_name': 'commit name',
            'patchset_files': [{'url': 'http://patchwork.server/patch/2322797/mbox/',
                                'name': 'mbox'}],
            'patchset_hash': '21c9a43d22cd02babb34b45a9defb881ae8228f0d034a0779b1321e851cad6a4',
            'message_id': '<e41888bcd8ecf2e9bc8cc37c56386e01a5b43c56.some-one@redhat.com>',
            'comment': 'this is the comment',
            'start_time': '2020-06-01T06:47:41.108Z',
            'valid': True,
            'contacts': ['someone@email.com', 'Some Other <some-other@mail.com>'],
            'log_url': 'http://log.server/log.name',
            'log_excerpt': 'Some\nLog\nLines',
            'misc': {
                'brew_task_id': 123,
                'submitter': 'someone@email.com',
                'scratch': True,
                'source_package_name': 'kernel',
                'all_sources_targeted': True,
                'patchset_modified_files': [
                    {'path': 'file/1'},
                    {'path': 'file/2'}
                ],
                'mr': {
                    'id': 9,
                    'url': 'https://url/to/merge/request',
                    'diff_url': 'https://url/to/merge/request',
                },
                'send_ready_for_test_pre': True,
                'send_ready_for_test_post': False,
            },
        }

        models.KCIDBCheckout.create_from_json(data)

        # Submit a base version without data to make sure resubmitting empty does not
        # override missing fields,
        checkout = models.KCIDBCheckout.create_from_json({
            'id': data['id'],
            'origin': data['origin']
        })

        self.assertEqual('redhat:1', checkout.id)
        self.assertIsInstance(checkout.origin, models.KCIDBOrigin)
        self.assertEqual('redhat', checkout.origin.name)

        self.assertIsInstance(checkout.tree, models.GitTree)
        self.assertEqual('arm', checkout.tree.name)

        self.assertEqual('https://repository.com/repo/kernel-rhel', checkout.git_repository_url)
        self.assertEqual('rhel-1.2.3', checkout.git_repository_branch)
        self.assertEqual('403cbf29a4e277ad4872515ec3854b175960bbdf', checkout.git_commit_hash)
        self.assertEqual('commit name', checkout.git_commit_name)
        self.assertEqual('<e41888bcd8ecf2e9bc8cc37c56386e01a5b43c56.some-one@redhat.com>', checkout.message_id)
        self.assertEqual('this is the comment', checkout.comment)
        self.assertEqual(True, checkout.valid)
        self.assertEqual('21c9a43d22cd02babb34b45a9defb881ae8228f0d034a0779b1321e851cad6a4', checkout.patchset_hash)
        self.assertEqual('Some\nLog\nLines', checkout.log_excerpt)
        self.assertEqual(models.KernelTypeEnum.INTERNAL, checkout.kernel_type)
        self.assertEqual(models.Policy.INTERNAL, checkout.policy.name)
        self.assertEqual(
            datetime.datetime(2020, 6, 1, 6, 47, 41, 108000, tzinfo=datetime.timezone.utc),
            checkout.start_time
        )

        self.assertEqual(123, checkout.brew_task_id)
        self.assertIsInstance(checkout.submitter, models.Maintainer)
        self.assertEqual('someone@email.com', checkout.submitter.email)
        self.assertEqual(True, checkout.scratch)
        self.assertEqual('kernel', checkout.source_package_name)
        self.assertEqual(True, checkout.all_sources_targeted)
        self.assertEqual([{'path': 'file/1'}, {'path': 'file/2'}], checkout.patchset_modified_files)
        self.assertEqual(
            {'id': 9, 'url': 'https://url/to/merge/request', 'diff_url': 'https://url/to/merge/request'},
            checkout.related_merge_request
        )
        self.assertEqual(True, checkout.notification_send_ready_for_test_pre)
        self.assertEqual(False, checkout.notification_send_ready_for_test_post)
        self.assertFalse(checkout.is_test_plan)

    def test_report_rules(self):
        """Submit report rules."""
        data = {
            'id': 'redhat:1',
            'origin': 'redhat',
            'misc': {
                'report_rules': '[{"when": "always", "send_to": "foo@bar.com"}]'
            }
        }

        checkout = models.KCIDBCheckout.create_from_json(data)
        self.assertEqual(
            [{'when': 'always', 'send_to': 'foo@bar.com'}],
            checkout.report_rules
        )

    @responses.activate
    def test_retrigger(self):
        """Check it sets the retrigger fields."""
        data = {
            'id': 'redhat:1',
            'origin': 'redhat',
            'misc': {
                'retrigger': True,
            },
        }

        checkout = models.KCIDBCheckout.create_from_json(data)

        self.assertEqual(models.Policy.RETRIGGER, checkout.policy.name)
        self.assertEqual(True, checkout.retrigger)

    def test_checkout_pointer(self):
        """Test checkout.checkout points to itself."""
        data = {
            'id': 'redhat:1',
            'origin': 'redhat',
        }
        checkout = models.KCIDBCheckout.create_from_json(data)
        self.assertEqual(checkout, checkout.checkout)

    def test_provenance(self):
        """Check provenance fields are correctly set."""
        data = {
            'id': 'redhat:1',
            'origin': 'redhat',
            'misc': {
                'provenance': [
                    {'url': 'http://1', 'function': 'coordinator'},
                    {'url': 'http://2', 'function': 'executor'},
                ]
            },
        }

        models.KCIDBCheckout.create_from_json(data)

        # Resubmit without provenance key, should not remove it
        del data['misc']['provenance']
        checkout = models.KCIDBCheckout.create_from_json(data)

        self.assertEqual(
            [(p.url, p.function) for p in checkout.provenance.all()],
            [('http://1', 'c'), ('http://2', 'e')],
        )


@mock.patch('datawarehouse.signals.kcidb_object.send', mock.Mock())
class TestKCIDBBuildFromJson(utils.TestCase):
    """Test creation of KCIDBBuild model."""

    @mock.patch('datawarehouse.signals.kcidb_object.send', mock.Mock())
    def setUp(self):
        """Set Up."""
        models.KCIDBCheckout.create_from_json(
            {
                'id': 'redhat:1',
                'origin': 'redhat',
            }
        )

    @mock.patch('datawarehouse.metrics.update_time_to_build')
    def test_basic(self, mock_update_time_to_build):
        """Submit only id."""
        data = {
            'checkout_id': 'redhat:1',
            'origin': 'redhat',
            'id': 'redhat:887318',
        }
        build = models.KCIDBBuild.create_from_json(data)

        self.assertIsInstance(build, models.KCIDBBuild)
        self.assertIsInstance(build.checkout, models.KCIDBCheckout)
        self.assertEqual('redhat:1', build.checkout.id)
        self.assertEqual('redhat:887318', build.id)
        self.assertIsInstance(build.origin, models.KCIDBOrigin)
        self.assertEqual('redhat', build.origin.name)

        mock_update_time_to_build.delay.assert_called_with(build.iid)

    def test_re_submit(self):
        """Submit submit multiple times. Updates current data."""
        data = {
            'checkout_id': 'redhat:1',
            'origin': 'redhat',
            'id': 'redhat:887318',
        }
        build = models.KCIDBBuild.create_from_json(data)
        self.assertIsNone(build.valid)

        # Re submit with new data.
        data['valid'] = False
        build = models.KCIDBBuild.create_from_json(data)
        self.assertEqual(build.valid, data['valid'])

        # Re submit with new data, without the previous value.
        del data['valid']
        data['comment'] = 'foobar'
        build = models.KCIDBBuild.create_from_json(data)
        self.assertIsNotNone(build.valid)
        self.assertEqual(build.comment, data['comment'])

        # Check there's only one instance
        self.assertEqual(
            1,
            models.KCIDBBuild.objects.filter(id=data['id']).count()
        )

    def test_submit_files(self):
        """Test files submission. log_url, input_files and output_files."""
        data = {
            'checkout_id': 'redhat:1',
            'origin': 'redhat',
            'id': 'redhat:887318',
            'log_url': 'http://log.server/log.name',
            'input_files': [
                {'url': 'http://log.server/input.file', 'name': 'input.file'},
                {'url': 'http://log.server/input.file.2', 'name': 'input.file.2'},
            ],
            'output_files': [
                {'url': 'http://log.server/output.file', 'name': 'output.file'},
                {'url': 'http://log.server/output.file.2', 'name': 'output.file.2'},
            ],
        }
        build = models.KCIDBBuild.create_from_json(data)

        # log_url
        self.assertIsInstance(build.log, models.Artifact)
        self.assertEqual('http://log.server/log.name', build.log.url)
        self.assertEqual('log.name', build.log.name)

        # input_files
        self.assertListEqual(
            [{'url': 'http://log.server/input.file', 'name': 'input.file'},
             {'url': 'http://log.server/input.file.2', 'name': 'input.file.2'}],
            [{'url': file.url, 'name': file.name} for file in build.input_files.all()]
        )

        # output_files
        self.assertListEqual(
            [{'url': 'http://log.server/output.file', 'name': 'output.file'},
             {'url': 'http://log.server/output.file.2', 'name': 'output.file.2'}],
            [{'url': file.url, 'name': file.name} for file in build.output_files.all()]
        )

    def test_re_submit_files(self):
        """Submit files multiple times."""
        data = {
            'checkout_id': 'redhat:1',
            'origin': 'redhat',
            'id': 'redhat:887318',
            'input_files': [
                {'url': 'http://log.server/input.file.1', 'name': 'input.file.1'},
                {'url': 'http://log.server/input.file.2', 'name': 'input.file.2'},
            ],
            'output_files': [
                {'url': 'http://log.server/output.file.1', 'name': 'output.file.1'},
                {'url': 'http://log.server/output.file.2', 'name': 'output.file.2'},
            ],
        }

        build = models.KCIDBBuild.create_from_json(data)
        self.assertEqual(
            set(build.input_files.all()),
            set(models.Artifact.objects.filter(name__in=['input.file.1', 'input.file.2']))
        )
        self.assertEqual(
            set(build.output_files.all()),
            set(models.Artifact.objects.filter(name__in=['output.file.1', 'output.file.2']))
        )

        # With new files, the current ones are replaced
        data.update({
            'input_files': [
                {'url': 'http://log.server/input.file.3', 'name': 'input.file.3'},
            ],
            'output_files': [
                {'url': 'http://log.server/output.file.3', 'name': 'output.file.3'},
            ],
        })

        build = models.KCIDBBuild.create_from_json(data)
        self.assertEqual(
            set(build.input_files.all()),
            set(models.Artifact.objects.filter(name='input.file.3'))
        )
        self.assertEqual(
            set(build.output_files.all()),
            set(models.Artifact.objects.filter(name='output.file.3'))
        )

        # If no files are provided, the files are not removed
        del data['input_files']
        del data['output_files']

        build = models.KCIDBBuild.create_from_json(data)
        self.assertEqual(build.input_files.count(), 1)
        self.assertEqual(build.output_files.count(), 1)

    def test_all_data(self):
        """Test complete object submission."""
        data = {
            'checkout_id': 'redhat:1',
            'origin': 'redhat',
            'id': 'redhat:887318',
            'log_url': 'http://log.server/log.name',
            'log_excerpt': 'Some\nLog\nLines',
            'input_files': [
                {'url': 'http://log.server/input.file', 'name': 'input.file'},
                {'url': 'http://log.server/input.file.2', 'name': 'input.file.2'},
            ],
            'output_files': [
                {'url': 'http://log.server/output.file', 'name': 'output.file'},
                {'url': 'http://log.server/output.file.2', 'name': 'output.file.2'},
            ],
            'start_time': '2020-06-03T15:14:57.215Z',
            'duration': 632,
            'architecture': 'aarch64',
            'command': 'make rpmbuild ...',
            'compiler': 'aarch64-linux-gnu-gcc (GCC) 8.2.1 20181105 (Red Hat Cross 8.2.1-1)',
            'config_name': 'fedora',
            'valid': True,
            'misc': {
                'test_plan_missing': True,
                'debug': True,
                'kpet_tree_name': 'kpet_tree_name',
                'kpet_tree_family': 'kpet_tree_family',
                'package_name': 'package_name',
                'testing_skipped_reason': 'unsupported',
            }
        }

        models.KCIDBBuild.create_from_json(data)

        # Submit a base version without data to make sure resubmitting empty does not
        # override missing fields,
        build = models.KCIDBBuild.create_from_json({
            'checkout_id': data['checkout_id'],
            'id': data['id'],
            'origin': data['origin']
        })

        self.assertEqual(
            datetime.datetime(2020, 6, 3, 15, 14, 57, 215000, tzinfo=datetime.timezone.utc),
            build.start_time
        )
        self.assertEqual(632, build.duration)
        self.assertEqual('make rpmbuild ...', build.command)
        self.assertEqual('fedora', build.config_name)
        self.assertEqual(True, build.valid)

        self.assertEqual(models.ArchitectureEnum['aarch64'], build.architecture)
        self.assertIsInstance(build.compiler, models.Compiler)
        self.assertEqual('aarch64-linux-gnu-gcc (GCC) 8.2.1 20181105 (Red Hat Cross 8.2.1-1)', build.compiler.name)
        self.assertEqual('Some\nLog\nLines', build.log_excerpt)
        self.assertTrue(build.test_plan_missing)
        self.assertTrue(build.debug)

        self.assertEqual('kpet_tree_name', build.kpet_tree_name)
        self.assertEqual('kpet_tree_family', build.kpet_tree_family)
        self.assertEqual('package_name', build.package_name)
        self.assertEqual('unsupported', build.testing_skipped_reason)
        self.assertFalse(build.is_test_plan)

    def test_test_plan_missing(self):
        """Test misc/test_plan_missing flag submission and update."""
        cases = [
            # misc, expected value
            ({}, False),
            ({'test_plan_missing': True}, True),
            ({'test_plan_missing': False}, False),
        ]

        for misc, expected in cases:
            data = {
                'checkout_id': 'redhat:1',
                'origin': 'redhat',
                'id': 'redhat:887318',
                'misc': misc,
            }
            build = models.KCIDBBuild.create_from_json(data)
            self.assertEqual(expected, build.test_plan_missing)

    def test_retrigger(self):
        """Check it sets the retrigger fields."""
        data = {
            'checkout_id': 'redhat:1',
            'origin': 'redhat',
            'id': 'redhat:887318',
            'misc': {
                'retrigger': True
            }
        }

        build = models.KCIDBBuild.create_from_json(data)
        self.assertTrue(build.retrigger)

    def test_provenance(self):
        """Check provenance fields are correctly set."""
        data = {
            'checkout_id': 'redhat:1',
            'origin': 'redhat',
            'id': 'redhat:2',
            'misc': {
                'provenance': [
                    {'url': 'http://1', 'function': 'coordinator'},
                    {'url': 'http://2', 'function': 'executor'},
                ]
            },
        }

        models.KCIDBBuild.create_from_json(data)

        # Resubmit without provenance key, should not remove it
        del data['misc']['provenance']
        build = models.KCIDBBuild.create_from_json(data)

        self.assertEqual(
            [(p.url, p.function) for p in build.provenance.all()],
            [('http://1', 'c'), ('http://2', 'e')],
        )


@mock.patch('datawarehouse.signals.kcidb_object.send', mock.Mock())
class TestKCIDBTestFromJson(utils.TestCase):
    """Test creation of KCIDBTest model."""

    @mock.patch('datawarehouse.signals.kcidb_object.send', mock.Mock())
    def setUp(self):
        """Set Up."""
        models.KCIDBCheckout.create_from_json(
            {
                'id': 'redhat:1',
                'origin': 'redhat',
            }
        )
        models.KCIDBBuild.create_from_json(
            {
                'checkout_id': 'redhat:1',
                'origin': 'redhat',
                'id': 'redhat:887318',
                'architecture': 'aarch64',
            }
        )

    def test_basic(self):
        """Submit only id."""
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
        }
        test = models.KCIDBTest.create_from_json(data)

        self.assertIsInstance(test, models.KCIDBTest)
        self.assertIsInstance(test.build, models.KCIDBBuild)
        self.assertEqual('redhat:887318', test.build.id)

        # These are False by default
        self.assertFalse(test.targeted)

    def test_re_submit(self):
        """Submit multiple times. Updates current data."""
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
        }
        test = models.KCIDBTest.create_from_json(data)
        self.assertIsNone(test.waived)

        # Re submit with new data.
        data['waived'] = True
        test = models.KCIDBTest.create_from_json(data)
        self.assertEqual(test.waived, data['waived'])

        # Re submit with new data, without the previous value.
        del data['waived']
        data['duration'] = 123
        test = models.KCIDBTest.create_from_json(data)
        self.assertIsNotNone(test.waived)
        self.assertEqual(test.duration, data['duration'])

        # Check there's only one instance
        self.assertEqual(
            1,
            models.KCIDBTest.objects.filter(id=data['id']).count()
        )

    def test_output_files(self):
        """Check created artifacts for output_files."""
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
            'output_files': [
                {'url': 'http://log.server/output.file', 'name': 'output.file'},
                {'url': 'http://log.server/output.file.2', 'name': 'output.file.2'},
            ],
        }
        test = models.KCIDBTest.create_from_json(data)

        self.assertListEqual(
            [
                {'url': 'http://log.server/output.file', 'name': 'output.file'},
                {'url': 'http://log.server/output.file.2', 'name': 'output.file.2'},
            ],
            [{'url': file.url, 'name': file.name} for file in test.output_files.all()]
        )

    def test_maintainers_empty(self):
        """Test maintainers field as comma separated string."""
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
            'comment': 'Boot test',
            'misc': {'maintainers': None}
        }
        models.KCIDBTest.create_from_json(data)

    def test_maintainers_not_empty(self):
        """Test maintainers field as dict."""
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
            'comment': 'Boot test',
            'misc': {
                'maintainers': [
                    {'name': 'Maintainer One', 'email': 'one@maintainer.com'},
                    {'email': 'two@maintainer.com', 'gitlab': 'two'},
                    {'email': 'three@maintainer.com'},
                ]
            }
        }
        test = models.KCIDBTest.create_from_json(data)

        cases = [
            ('Maintainer One', 'one@maintainer.com', None),
            ('', 'two@maintainer.com', 'two'),
            ('', 'three@maintainer.com', None),
        ]

        for name, email, gitlab_username in cases:
            self.assertTrue(
                test.test.maintainers.filter(
                    name=name, email=email, gitlab_username=gitlab_username).exists(),
                (name, email)
            )

    def test_maintainers_overwrite(self):
        """Test behaviour with empty maintainers list. Should not overwrite."""
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
            'comment': 'Boot test',
            'misc': {
                'maintainers': [
                    {'name': 'Maintainer One', 'email': 'one@maintainer.com'},
                ]
            }
        }
        models.KCIDBTest.create_from_json(data)

        # Resubmit test without maintainers
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
            'comment': 'Boot test',
        }
        kcidb_test = models.KCIDBTest.create_from_json(data)

        self.assertEqual(
            'Maintainer One',
            kcidb_test.test.maintainers.get().name
        )

    def test_all_data(self):
        """Test complete object submission."""
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
            'environment': {
                'comment': 'hostname.redhat.com'
            },
            'path': 'boot',
            'comment': 'Boot test',
            'waived': False,
            'start_time': '2020-06-03T15:52:25Z',
            'duration': 158,
            'status': 'PASS',
            'log_url': 'http://log.server/log.name',
            'log_excerpt': 'Some\nLog\nLines',
            'output_files': [
                {'url': 'http://log.server/output.file', 'name': 'output.file'},
            ],
            'misc': {
                'maintainers': [
                    {'name': 'Cosme Fulanito', 'email': 'cosme@fulanito.com'},
                    {'name': 'Pepe', 'email': 'pe@pe.com'}
                ],
                'targeted': True,
                'fetch_url': 'http://test.url',
            }
        }
        models.KCIDBTest.create_from_json(data)

        # Submit a base version without data to make sure resubmitting empty does not
        # override missing fields,
        test = models.KCIDBTest.create_from_json({
            'build_id': data['build_id'],
            'id': data['id'],
            'origin': data['origin'],
        })

        self.assertIsInstance(test.environment, models.BeakerResource)
        self.assertEqual('hostname.redhat.com', test.environment.fqdn)

        self.assertEqual('P', test.status)

        self.assertIsInstance(test.test, models.Test)
        self.assertEqual('boot', test.test.universal_id)
        self.assertEqual('Boot test', test.test.name)
        self.assertEqual(2, test.test.maintainers.count())
        self.assertTrue(
            test.test.maintainers.filter(name='Cosme Fulanito', email='cosme@fulanito.com').exists()
        )
        self.assertTrue(
            test.test.maintainers.filter(name='Pepe', email='pe@pe.com').exists()
        )
        self.assertEqual('http://test.url', test.test.fetch_url)

        self.assertEqual(False, test.waived)
        self.assertEqual(
            datetime.datetime(2020, 6, 3, 15, 52, 25, 0, tzinfo=datetime.timezone.utc),
            test.start_time
        )
        self.assertEqual(158, test.duration)

        self.assertEqual(1, test.output_files.count())
        self.assertTrue(test.targeted)

        self.assertEqual('http://log.server/log.name', test.log.url)
        self.assertEqual('log.name', test.log.name)
        self.assertEqual('Some\nLog\nLines', test.log_excerpt)
        self.assertFalse(test.is_test_plan)

    def test_re_submit_files(self):
        """Submit files multiple times."""
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
            'output_files': [
                {'url': 'http://log.server/output.file', 'name': 'output.file'},
                {'url': 'http://log.server/output.file.2', 'name': 'output.file.2'},
            ],
        }
        test = models.KCIDBTest.create_from_json(data)
        self.assertEqual(
            set(test.output_files.all()),
            set(models.Artifact.objects.filter(name__in=['output.file', 'output.file.2']))
        )

        # With new files, the current ones are replaced
        data.update({
            'output_files': [
                {'url': 'http://log.server/output.file.3', 'name': 'output.file.3'},
            ],
        })

        test = models.KCIDBTest.create_from_json(data)
        self.assertEqual(
            set(test.output_files.all()),
            set(models.Artifact.objects.filter(name='output.file.3'))
        )

        # If no files are provided, the files are not removed
        del data['output_files']

        test = models.KCIDBTest.create_from_json(data)
        self.assertEqual(test.output_files.count(), 1)

    def test_retrigger(self):
        """Check it sets the retrigger fields."""
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
            'misc': {
                'retrigger': True,
            }
        }
        test = models.KCIDBTest.create_from_json(data)

        self.assertTrue(test.retrigger)

    def test_checkout_pointer(self):
        """Test test.checkout points to the checkout."""
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
        }
        test = models.KCIDBTest.create_from_json(data)
        self.assertEqual(test.build.checkout, test.checkout)

    def test_provenance(self):
        """Check provenance fields are correctly set."""
        data = {
            'build_id': 'redhat:887318',
            'origin': 'redhat',
            'id': 'redhat:111218982',
            'misc': {
                'provenance': [
                    {'url': 'http://1', 'function': 'coordinator'},
                    {'url': 'http://2', 'function': 'executor'},
                ]
            },
        }

        models.KCIDBTest.create_from_json(data)

        # Resubmit without provenance key, should not remove it
        del data['misc']['provenance']
        test = models.KCIDBTest.create_from_json(data)

        self.assertEqual(
            [(p.url, p.function) for p in test.provenance.all()],
            [('http://1', 'c'), ('http://2', 'e')],
        )


class TestKCIDBNotifications(utils.TestCase):
    """Test kcidb notifications are sent only when the object is created or updated."""

    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_checkout_new(self, signal):
        """Test send_kcidb_notification when the checkout is created."""
        data = {
            'id': 'redhat:1',
            'origin': 'redhat',
        }

        # Create call
        new = models.KCIDBCheckout.create_from_json(data)

        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.checkout.created_object_send_message',
                status='new',
                object_type='checkout',
                objects=[new]
            ),
        ])

        self.assertEqual(1, signal.send.call_count)

    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_checkout_updated_test_plan(self, signal):
        """Test send_kcidb_notification when the test plan is updated."""
        data = {
            'id': 'redhat:1',
            'origin': 'redhat',
        }

        # Create call
        new = models.KCIDBCheckout.create_from_json(data)
        # Update call
        models.KCIDBCheckout.create_from_json(data)

        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.checkout.created_object_send_message',
                status='new',
                object_type='checkout',
                objects=[new]
            ),
        ])

        self.assertEqual(1, signal.send.call_count)

    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_checkout_updated_without_changes(self, signal):
        """Test send_kcidb_notification when the checkout is updated without changes."""
        data = {
            'id': 'redhat:1',
            'origin': 'redhat',
            'comment': 'comment',
            'valid': True,
        }

        # Create call
        new = models.KCIDBCheckout.create_from_json(data)
        # Update call
        models.KCIDBCheckout.create_from_json(data)

        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.checkout.created_object_send_message',
                status='new',
                object_type='checkout',
                objects=[new]
            ),
        ])

        self.assertEqual(1, signal.send.call_count)

    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_checkout_updated_partially_without_changes(self, signal):
        """Test send_kcidb_notification when the checkout is updated partially without changes."""
        data = {
            'id': 'redhat:1',
            'origin': 'redhat',
            'comment': 'comment',
            'valid': True,
        }

        partial_data = {
            'id': 'redhat:1',
            'origin': 'redhat',
            'valid': True,
        }

        # Create call
        new = models.KCIDBCheckout.create_from_json(data)
        # Update call
        models.KCIDBCheckout.create_from_json(partial_data)

        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.checkout.created_object_send_message',
                status='new',
                object_type='checkout',
                objects=[new]
            ),
        ])

        self.assertEqual(1, signal.send.call_count)

    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_checkout_updated_with_changes(self, signal):
        """Test send_kcidb_notification when the checkout is updated with changes."""
        data = {
            'id': 'redhat:1',
            'origin': 'redhat',
            'comment': 'comment',
            'valid': False,
        }

        # Create call
        new = models.KCIDBCheckout.create_from_json(data)
        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.checkout.created_object_send_message',
                status='new',
                object_type='checkout',
                objects=[new]
            ),
        ])

        self.assertEqual(1, signal.send.call_count)

        signal.send.reset_mock()

        # Update call
        data['comment'] = 'Comment'
        updated = models.KCIDBCheckout.create_from_json(data)

        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.checkout.created_object_send_message',
                status='updated',
                object_type='checkout',
                objects=[updated]
            ),
        ])

        self.assertEqual(1, signal.send.call_count)

    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_build_new(self, signal):
        """Test send_kcidb_notification when the build is created."""
        rev = models.KCIDBCheckout.objects.create(
            id='redhat:1',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        data = {
            'checkout_id': rev.id,
            'origin': 'redhat',
            'id': 'redhat:887318',
        }

        # Create call
        build = models.KCIDBBuild.create_from_json(data)

        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.build.created_object_send_message',
                status='new',
                object_type='build',
                objects=[build]
            ),
        ])
        self.assertEqual(1, signal.send.call_count)

    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_build_updated_test_plan(self, signal):
        """Test send_kcidb_notification when the test plan is updated."""
        rev = models.KCIDBCheckout.objects.create(
            id='redhat:1',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        data = {
            'checkout_id': rev.id,
            'origin': 'redhat',
            'id': 'redhat:887318',
        }

        # Create call
        new = models.KCIDBBuild.create_from_json(data)
        # Update call
        models.KCIDBBuild.create_from_json(data)

        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.build.created_object_send_message',
                status='new',
                object_type='build',
                objects=[new]
            ),
        ])

        self.assertEqual(1, signal.send.call_count)

    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_build_updated_partially_without_changes(self, signal):
        """Test send_kcidb_notification when the build is partially updated without changes."""
        rev = models.KCIDBCheckout.objects.create(
            id='redhat:1',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        data = {
            'checkout_id': rev.id,
            'origin': 'redhat',
            'id': 'redhat:887318',
            'comment': 'comment',
            'valid': False,
        }

        partial_data = {
            'checkout_id': rev.id,
            'origin': 'redhat',
            'id': 'redhat:887318',
            'valid': False,
        }

        # Create call
        new = models.KCIDBBuild.create_from_json(data)
        # Update call
        models.KCIDBBuild.create_from_json(partial_data)

        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.build.created_object_send_message',
                status='new',
                object_type='build',
                objects=[new]
            ),
        ])

        self.assertEqual(1, signal.send.call_count)

    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_build_updated_without_changes(self, signal):
        """Test send_kcidb_notification when the build is updated without changes."""
        rev = models.KCIDBCheckout.objects.create(
            id='redhat:1',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        data = {
            'checkout_id': rev.id,
            'origin': 'redhat',
            'id': 'redhat:887318',
            'valid': False,
        }

        # Create call
        new = models.KCIDBBuild.create_from_json(data)
        # Update call
        models.KCIDBBuild.create_from_json(data)

        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.build.created_object_send_message',
                status='new',
                object_type='build',
                objects=[new]
            ),
        ])

        self.assertEqual(1, signal.send.call_count)

    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_build_updated_with_changes(self, signal):
        """Test send_kcidb_notification when the build is updated with changes."""
        rev = models.KCIDBCheckout.objects.create(
            id='redhat:1',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        data = {
            'checkout_id': rev.id,
            'origin': 'redhat',
            'id': 'redhat:887318',
            'valid': False,
        }

        # Create call
        new = models.KCIDBBuild.create_from_json(data)

        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.build.created_object_send_message',
                status='new',
                object_type='build',
                objects=[new]
            ),
        ])

        self.assertEqual(1, signal.send.call_count)

        signal.send.reset_mock()
        # Update call
        data['comment'] = 'Comment'
        updated = models.KCIDBBuild.create_from_json(data)

        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.build.created_object_send_message',
                status='updated',
                object_type='build',
                objects=[updated]
            ),
        ])

        self.assertEqual(1, signal.send.call_count)

    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_test_new(self, signal):
        """Test send_kcidb_notification when the test is created."""
        rev = models.KCIDBCheckout.objects.create(
            id='redhat:1',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        models.KCIDBBuild.objects.create(
            checkout=rev,
            id='redhat:887318',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )

        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
        }

        # Create call
        new = models.KCIDBTest.create_from_json(data)

        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.test.created_object_send_message',
                status='new',
                object_type='test',
                objects=[new]
            ),
        ])

        self.assertEqual(1, signal.send.call_count)

    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_test_updated_test_plan(self, signal):
        """Test send_kcidb_notification when the test plan is updated."""
        rev = models.KCIDBCheckout.objects.create(
            id='redhat:1',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        models.KCIDBBuild.objects.create(
            checkout=rev,
            id='redhat:887318',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )

        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
        }

        # Create call
        new = models.KCIDBTest.create_from_json(data)
        # Update call
        models.KCIDBTest.create_from_json(data)

        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.test.created_object_send_message',
                status='new',
                object_type='test',
                objects=[new]
            ),
        ])

        self.assertEqual(1, signal.send.call_count)

    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_test_updated_without_changes(self, signal):
        """Test send_kcidb_notification when the test is updated without changes."""
        rev = models.KCIDBCheckout.objects.create(
            id='redhat:1',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        models.KCIDBBuild.objects.create(
            checkout=rev,
            id='redhat:887318',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )

        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
            'waived': True,
        }

        # Create call
        new = models.KCIDBTest.create_from_json(data)
        # Update call
        models.KCIDBTest.create_from_json(data)

        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.test.created_object_send_message',
                status='new',
                object_type='test',
                objects=[new]
            ),
        ])

        self.assertEqual(1, signal.send.call_count)

    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_test_updated_partially_without_changes(self, signal):
        """Test send_kcidb_notification when the test is updated partially without changes."""
        rev = models.KCIDBCheckout.objects.create(
            id='redhat:1',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        models.KCIDBBuild.objects.create(
            checkout=rev,
            id='redhat:887318',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )

        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
            'waived': True,
        }

        partial_data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
        }

        # Create call
        new = models.KCIDBTest.create_from_json(data)
        # Update call
        models.KCIDBTest.create_from_json(partial_data)

        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.test.created_object_send_message',
                status='new',
                object_type='test',
                objects=[new]
            ),
        ])

        self.assertEqual(1, signal.send.call_count)

    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_test_updated_with_changes(self, signal):
        """Test send_kcidb_notification when the test is updated with changes."""
        rev = models.KCIDBCheckout.objects.create(
            id='redhat:1',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        models.KCIDBBuild.objects.create(
            checkout=rev,
            id='redhat:887318',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )

        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
            'waived': False,
        }

        # Create call
        new = models.KCIDBTest.create_from_json(data)

        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.test.created_object_send_message',
                status='new',
                object_type='test',
                objects=[new]
            ),
        ])

        self.assertEqual(1, signal.send.call_count)

        signal.send.reset_mock()

        # Update call
        data['waived'] = True
        updated = models.KCIDBTest.create_from_json(data)

        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.test.created_object_send_message',
                status='updated',
                object_type='test',
                objects=[updated]
            ),
        ])

        self.assertEqual(1, signal.send.call_count)


class TestVisibility(utils.TestCase):
    """Test objects visibility."""

    def test_checkout(self):
        """Test checkout visibility."""
        checkout = models.KCIDBCheckout.objects.create(
            id='decd6167bf4f6bec1284006d0522381b44660df3',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )

        # By default is private.
        self.assertFalse(checkout.is_public)

        checkout.kernel_type = models.KernelTypeEnum.INTERNAL
        checkout.save()
        self.assertFalse(checkout.is_public)

        checkout.kernel_type = models.KernelTypeEnum.UPSTREAM
        checkout.save()
        self.assertTrue(checkout.is_public)

    def test_build(self):
        """Test build visibility."""
        rev = models.KCIDBCheckout.objects.create(
            id='decd6167bf4f6bec1284006d0522381b44660df3',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        build = models.KCIDBBuild.create_from_json({
            'checkout_id': rev.id,
            'origin': 'redhat',
            'id': 'redhat:887318',
        })

        # By default is private.
        self.assertFalse(build.is_public)

        build.checkout.kernel_type = models.KernelTypeEnum.INTERNAL
        build.checkout.save()
        self.assertFalse(build.is_public)

        build.checkout.kernel_type = models.KernelTypeEnum.UPSTREAM
        build.checkout.save()
        self.assertTrue(build.is_public)

    def test_test(self):
        """Test test visibility."""
        rev = models.KCIDBCheckout.objects.create(
            id='decd6167bf4f6bec1284006d0522381b44660df3',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        models.KCIDBBuild.objects.create(
            checkout=rev,
            id='redhat:887318',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )

        test = models.KCIDBTest.create_from_json({
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
        })

        # By default is private.
        self.assertFalse(test.is_public)

        test.build.checkout.kernel_type = models.KernelTypeEnum.INTERNAL
        test.build.checkout.save()
        self.assertFalse(test.is_public)

        test.build.checkout.kernel_type = models.KernelTypeEnum.UPSTREAM
        test.build.checkout.save()
        self.assertTrue(test.is_public)


class TestCheckoutAggregated(utils.TestCase):
    """Test aggregated data on a checkout."""

    def setUp(self):
        data = {
            'version': {'major': 4, 'minor': 0},
            'checkouts': [
                {'origin': 'redhat', 'id': 'redhat:decd6167bf4f6bec1284006d0522381b44660df3', 'valid': False},
            ],
            'builds': [
                {'origin': 'redhat', 'checkout_id': 'redhat:decd6167bf4f6bec1284006d0522381b44660df3',
                 'id': 'redhat:build-1', 'valid': False},
                {'origin': 'redhat', 'checkout_id': 'redhat:decd6167bf4f6bec1284006d0522381b44660df3',
                 'id': 'redhat:build-2', 'valid': False},
                {'origin': 'redhat', 'checkout_id': 'redhat:decd6167bf4f6bec1284006d0522381b44660df3',
                 'id': 'redhat:build-3', 'valid': True},
            ],
            'tests': [
                {'origin': 'redhat', 'build_id': 'redhat:build-1', 'id': 'redhat:test-1', 'status': 'FAIL'},
                {'origin': 'redhat', 'build_id': 'redhat:build-1', 'id': 'redhat:test-2', 'status': 'ERROR'},
                {'origin': 'redhat', 'build_id': 'redhat:build-1', 'id': 'redhat:test-3', 'status': 'PASS'},
            ]
        }
        self.assert_authenticated_post(
            201, 'add_kcidbcheckout', '/api/1/kcidb/submit', json.dumps({'data': data}),
            content_type="application/json"
        )

    def _test_rev(self, cases, only_aggregated=False):
        """Test attrs on both rev and rev_aggregated."""
        rev_id = 'redhat:decd6167bf4f6bec1284006d0522381b44660df3'
        rev = models.KCIDBCheckout.objects.get(id=rev_id)
        rev_aggregated = models.KCIDBCheckout.objects.aggregated().get(id=rev_id)

        for attr, value in cases:
            if isinstance(value, QuerySet):
                check = self.assertQuerySetEqual
            else:
                check = self.assertEqual

            check(getattr(rev_aggregated, attr), value, attr)
            if not only_aggregated:
                check(getattr(rev, attr), value, attr)

    def test_tests_errors(self):
        """Test stats_tests_errors aggregated property."""
        # Set all as Pass
        models.KCIDBTest.objects.update(status=models.ResultEnum.PASS)
        self._test_rev([('stats_tests_errors', False)], only_aggregated=True)
        # Set only one as Error
        models.KCIDBTest.objects.filter(id='redhat:test-1').update(status=models.ResultEnum.ERROR)
        self._test_rev([('stats_tests_errors', True)], only_aggregated=True)

    def test_nothing_triaged(self):
        """Test none objects were triaged."""
        rev = models.KCIDBCheckout.objects.get(id='redhat:decd6167bf4f6bec1284006d0522381b44660df3')

        # Without aggregated call these methods are not available.
        self.assertFalse(hasattr(rev, 'stats_checkout_triaged'))
        self.assertFalse(hasattr(rev, 'stats_checkout_untriaged'))
        self.assertFalse(hasattr(rev, 'stats_tests_triaged'))
        self.assertFalse(hasattr(rev, 'stats_tests_untriaged'))
        self.assertFalse(hasattr(rev, 'stats_builds_triaged'))
        self.assertFalse(hasattr(rev, 'stats_builds_untriaged'))
        self.assertIsNone(rev.has_objects_missing_triage)
        self.assertIsNone(rev.has_objects_with_issues)

        # No issues but failed jobs. All untriaged, no triaged.
        cases = [
            ('stats_checkout_untriaged', True),
            ('stats_builds_untriaged', True),
            ('stats_tests_untriaged', True),
            ('stats_checkout_triaged', False),
            ('stats_builds_triaged', False),
            ('stats_tests_triaged', False),
            # No issues, missing triage.
            ('has_objects_missing_triage', True),
            ('has_objects_with_issues', False),
        ]
        self._test_rev(cases, only_aggregated=True)

        cases = [
            # Check list of triaged and untriaged jobs.
            ('builds_triaged', models.KCIDBBuild.objects.none()),
            ('builds_untriaged', models.KCIDBBuild.objects.filter(id__in=('redhat:build-1', 'redhat:build-2'))),
            ('tests_triaged', models.KCIDBTest.objects.none()),
            ('tests_untriaged', models.KCIDBTest.objects.filter(id__in=('redhat:test-1', 'redhat:test-2'))),
            # Checkout is not triaged.
            ('is_triaged', False),
            ('is_missing_triage', True),
        ]
        self._test_rev(cases)

    def test_partially_triaged(self):
        """Test some objects were triaged and some others not."""
        # Add some issues to some builds and tests.
        issue = models.Issue.objects.create(
            kind=models.IssueKind.objects.create(description="fail 1", tag="1"),
            description='foo bar',
            ticket_url='http://some.url',
        )
        models.KCIDBCheckout.objects.get(id='redhat:decd6167bf4f6bec1284006d0522381b44660df3').issues.add(issue)
        models.KCIDBBuild.objects.get(id='redhat:build-1').issues.add(issue)
        models.KCIDBTest.objects.get(id='redhat:test-1').issues.add(issue)

        # We now have both triaged and untriaged jobs.
        cases = [
            ('stats_checkout_triaged', True),
            ('stats_checkout_untriaged', False),
            ('stats_builds_triaged', True),
            ('stats_builds_untriaged', True),
            ('stats_tests_triaged', True),
            ('stats_tests_untriaged', True),
            # Some issues, missing triage.
            ('has_objects_missing_triage', True),
            ('has_objects_with_issues', True),
        ]
        self._test_rev(cases, only_aggregated=True)

        cases = [
            # Check list of triaged and untriaged jobs.
            ('builds_triaged', models.KCIDBBuild.objects.filter(id='redhat:build-1')),
            ('builds_untriaged', models.KCIDBBuild.objects.filter(id='redhat:build-2')),
            ('tests_triaged', models.KCIDBTest.objects.filter(id='redhat:test-1')),
            ('tests_untriaged', models.KCIDBTest.objects.filter(id='redhat:test-2')),
            # Checkout is triaged.
            ('is_triaged', True),
            ('is_missing_triage', False),
        ]
        self._test_rev(cases)

    def test_fully_triaged(self):
        """Test all objects were triaged."""
        # Add issue to all failures.
        issue = models.Issue.objects.create(
            kind=models.IssueKind.objects.create(description="fail 1", tag="1"),
            description='foo bar',
            ticket_url='http://some.url',
        )
        models.KCIDBCheckout.objects.get(id='redhat:decd6167bf4f6bec1284006d0522381b44660df3').issues.add(issue)
        models.KCIDBBuild.objects.get(id='redhat:build-1').issues.add(issue)
        models.KCIDBBuild.objects.get(id='redhat:build-2').issues.add(issue)
        models.KCIDBTest.objects.get(id='redhat:test-1').issues.add(issue)
        models.KCIDBTest.objects.get(id='redhat:test-2').issues.add(issue)

        # We now have all issues triaged.
        cases = [
            ('stats_builds_triaged', True),
            ('stats_builds_untriaged', False),
            ('stats_tests_triaged', True),
            ('stats_tests_untriaged', False),
            # No failures missing triage.
            ('has_objects_missing_triage', False),
            ('has_objects_with_issues', True),
        ]
        self._test_rev(cases, only_aggregated=True)

        cases = [
            # Check list of triaged and untriaged jobs.
            ('builds_untriaged', models.KCIDBBuild.objects.none()),
            ('builds_triaged', models.KCIDBBuild.objects.filter(id__in=('redhat:build-1', 'redhat:build-2'))),
            ('tests_untriaged', models.KCIDBTest.objects.none()),
            ('tests_triaged', models.KCIDBTest.objects.filter(id__in=('redhat:test-1', 'redhat:test-2')))
        ]
        self._test_rev(cases)


class TestKCIDBProperties(utils.TestCase):
    """Test objects properties."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
        'tests/kcidb/fixtures/issues.yaml',
    ]

    def test_checkout_web_url(self):
        """Test KCIDBCheckout's web_url property."""
        checkout = models.KCIDBCheckout.objects.first()
        with mock.patch('datawarehouse.models.kcidb_models.settings.DATAWAREHOUSE_URL', 'http://dw'):
            self.assertEqual(f'http://dw/kcidb/checkouts/{checkout.iid}', checkout.web_url)

    def test_build_web_url(self):
        """Test KCIDBBuild's web_url property."""
        build = models.KCIDBBuild.objects.first()
        with mock.patch('datawarehouse.models.kcidb_models.settings.DATAWAREHOUSE_URL', 'http://dw'):
            self.assertEqual(f'http://dw/kcidb/builds/{build.iid}', build.web_url)

    def test_test_web_url(self):
        """Test KCIDBTest's web_url property."""
        test = models.KCIDBTest.objects.first()
        with mock.patch('datawarehouse.models.kcidb_models.settings.DATAWAREHOUSE_URL', 'http://dw'):
            self.assertEqual(f'http://dw/kcidb/tests/{test.iid}', test.web_url)

    def test_checkout_is_missing_triage(self):
        """Test KCIDBCheckout's is_missing_triage property."""
        checkout = models.KCIDBCheckout.objects.first()

        # Valid, triage not needed.
        checkout.valid = True
        self.assertFalse(checkout.is_missing_triage)

        # Invalid, missing triage.
        checkout.valid = False
        self.assertTrue(checkout.is_missing_triage)

        # Issue tagged, triage done.
        models.IssueOccurrence.objects.create(
            issue=models.Issue.objects.first(),
            kcidb_checkout=checkout
        )
        self.assertFalse(checkout.is_missing_triage)

    def test_build_is_missing_triage(self):
        """Test KCIDBBuild's is_missing_triage property."""
        build = models.KCIDBBuild.objects.first()

        # Valid, triage not needed.
        build.valid = True
        self.assertFalse(build.is_missing_triage)

        # Invalid, missing triage.
        build.valid = False
        self.assertTrue(build.is_missing_triage)

        # Issue tagged, triage done.
        models.IssueOccurrence.objects.create(
            issue=models.Issue.objects.first(),
            kcidb_build=build
        )
        self.assertFalse(build.is_missing_triage)

    def test_test_is_missing_triage(self):
        """Test KCIDBTest's is_missing_triage property."""
        test = models.KCIDBTest.objects.first()

        # Passed, triage not needed.
        test.status = 'P'
        self.assertFalse(test.is_missing_triage)

        # Failed, missing triage.
        test.status = 'F'
        self.assertTrue(test.is_missing_triage)

        # Issue tagged, triage done.
        models.IssueOccurrence.objects.create(
            issue=models.Issue.objects.first(),
            kcidb_test=test
        )
        self.assertFalse(test.is_missing_triage)


class TestBuildAggregated(utils.TestCase):
    """Test aggregated data on a build."""

    def setUp(self):
        data = {
            'version': {'major': 4, 'minor': 0},
            'checkouts': [
                {'origin': 'redhat', 'id': 'redhat:decd6167bf4f6bec1284006d0522381b44660df3', 'valid': False},
            ],
            'builds': [
                {'origin': 'redhat', 'checkout_id': 'redhat:decd6167bf4f6bec1284006d0522381b44660df3',
                 'id': 'redhat:build-1', 'valid': False},
            ],
            'tests': [
                {'origin': 'redhat', 'build_id': 'redhat:build-1', 'id': 'redhat:test-1', 'status': 'FAIL'},
                {'origin': 'redhat', 'build_id': 'redhat:build-1', 'id': 'redhat:test-2', 'status': 'ERROR'},
                {'origin': 'redhat', 'build_id': 'redhat:build-1', 'id': 'redhat:test-3', 'status': 'PASS'},
            ]
        }
        self.assert_authenticated_post(
            201, 'add_kcidbcheckout', '/api/1/kcidb/submit', json.dumps({'data': data}),
            content_type="application/json"
        )

    def _test_build(self, cases, only_aggregated=False):
        """Test attrs on both build and build_aggregated."""
        build_id = 'redhat:build-1'
        build = models.KCIDBBuild.objects.get(id=build_id)
        build_aggregated = models.KCIDBBuild.objects.aggregated().get(id=build_id)

        for attr, value in cases:
            if isinstance(value, QuerySet):
                check = self.assertQuerySetEqual
            else:
                check = self.assertEqual

            check(getattr(build_aggregated, attr), value, attr)
            if not only_aggregated:
                check(getattr(build, attr), value, attr)

    def test_tests_errors(self):
        """Test stats_tests_errors aggregated property."""
        # Set all as Pass
        models.KCIDBTest.objects.update(status=models.ResultEnum.PASS)
        self._test_build([('has_tests_errors', False)])
        # Set only one as Error
        models.KCIDBTest.objects.filter(id='redhat:test-1').update(status=models.ResultEnum.ERROR)
        self._test_build([('has_tests_errors', True)])


class TestCheckoutAnnotatedByArchitecture(utils.TestCase):
    """Test annotated_by_architecture data on a checkout."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/issues.yaml',
        'tests/kcidb/fixtures/base_multiple_architectures.yaml',
    ]

    def test_counters(self):
        """Test annotated_by_architecture counters."""
        rev = (
            models.KCIDBCheckout.objects
            .annotated_by_architecture()
            .get(id='redhat:d1c47b385764aaa488bce182d944fa22bb1325d1')
        )

        test_cases = {
            'aarch64': {
                'builds_ran': 1,
                'builds_failed': 0,
                'builds_failed_untriaged': 0,
                'builds_with_issues': 0,
                'tests_ran': 1,
                'tests_failed': 1,
                'tests_failed_untriaged': 1,
                'tests_failed_waived': 0,
                'tests_with_issues': 0,
            },
            'ppc64': {
                'builds_ran': 1,
                'builds_failed': 0,
                'builds_failed_untriaged': 0,
                'builds_with_issues': 0,
                'tests_ran': 1,
                'tests_failed': 0,
                'tests_failed_untriaged': 0,
                'tests_failed_waived': 0,
                'tests_with_issues': 0,
            },
            'ppc64le': {
                'builds_ran': 1,
                'builds_failed': 1,
                'builds_failed_untriaged': 0,
                'builds_with_issues': 1,
                'tests_ran': 3,
                'tests_failed': 1,
                'tests_failed_untriaged': 0,
                'tests_failed_waived': 1,
                'tests_with_issues': 2,
            },
        }

        for arch, fields in test_cases.items():
            for key, value in fields.items():
                self.assertEqual(getattr(rev, f'stats_{arch}_{key}_count'), value, (arch, key))

        # All architectures not included in test_case should be zero.
        for arch in models.ArchitectureEnum:
            if arch.name in test_cases:
                continue

            for key in test_cases['aarch64']:
                self.assertEqual(getattr(rev, f'stats_{arch.name}_{key}_count'), 0, (arch.name, key))

    def test_annotated_by_architeture(self):
        """Test annotated_by_architecture."""
        checkout = (
            models.KCIDBCheckout.objects
            .annotated_by_architecture()
            .get(id='redhat:d1c47b385764aaa488bce182d944fa22bb1325d1')
        )

        for arch in models.ArchitectureEnum:
            self.assertEqual(
                checkout.annotated_by_architecture[arch.name]['builds']['ran'],
                getattr(checkout, f'stats_{arch.name}_builds_ran_count'))
            self.assertEqual(
                checkout.annotated_by_architecture[arch.name]['builds']['failed'],
                getattr(checkout, f'stats_{arch.name}_builds_failed_count'))
            self.assertEqual(
                checkout.annotated_by_architecture[arch.name]['tests']['ran'],
                getattr(checkout, f'stats_{arch.name}_tests_ran_count'))
            self.assertEqual(
                checkout.annotated_by_architecture[arch.name]['tests']['failed'],
                getattr(checkout, f'stats_{arch.name}_tests_failed_count'))
            self.assertEqual(
                checkout.annotated_by_architecture[arch.name]['tests']['failed_waived'],
                getattr(checkout, f'stats_{arch.name}_tests_failed_waived_count'))
            self.assertEqual(
                checkout.annotated_by_architecture[arch.name]['known_issues'],
                (
                    getattr(checkout, f'stats_{arch.name}_builds_with_issues_count') +
                    getattr(checkout, f'stats_{arch.name}_tests_with_issues_count')
                )
            )


class TestCheckoutFilterReadyToReport(utils.TestCase):
    """Unit tests for KCIDBCheckoutManager.filter_ready_to_report."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
    ]

    def test_already_ready_to_report(self):
        """All checkouts are tagged as ready_to_report."""
        models.KCIDBCheckout.objects.update(ready_to_report=True)

        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_ready_to_report()
        )

    def test_checkouts_not_finished(self):
        """Checkouts have no valid value."""
        models.KCIDBCheckout.objects.update(valid=None)

        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_ready_to_report()
        )

    def test_builds_not_finished(self):
        """Builds have no valid value."""
        models.KCIDBBuild.objects.update(valid=None)

        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_ready_to_report()
        )

    def test_tests_not_finished(self):
        """Tests have no status value."""
        models.KCIDBTest.objects.update(status=None)

        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_ready_to_report()
        )

    def test_not_triaged(self):
        """All objects finished but were not triaged."""
        models.KCIDBCheckout.objects.update(valid=True)
        models.KCIDBBuild.objects.update(valid=True)
        models.KCIDBTest.objects.update(status='P')

        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_ready_to_report()
        )

    def test_ready(self):
        """All objects finished and were triaged."""
        models.KCIDBCheckout.objects.update(valid=True, last_triaged_at=timezone.now())
        models.KCIDBBuild.objects.update(valid=True, last_triaged_at=timezone.now())
        models.KCIDBTest.objects.update(status='P', last_triaged_at=timezone.now())

        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.all(),
            models.KCIDBCheckout.objects.filter_ready_to_report()
        )

    def test_ready_and_reported(self):
        """All objects finished, were triaged but already reported."""
        models.KCIDBCheckout.objects.update(valid=True, last_triaged_at=timezone.now(), ready_to_report=True)
        models.KCIDBBuild.objects.update(valid=True, last_triaged_at=timezone.now())
        models.KCIDBTest.objects.update(status='P', last_triaged_at=timezone.now())

        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_ready_to_report()
        )

    def test_test_plan_missing(self):
        """All objects finished, but a build still has test plan missing."""
        models.KCIDBCheckout.objects.update(valid=True, last_triaged_at=timezone.now())
        models.KCIDBBuild.objects.update(valid=True, last_triaged_at=timezone.now(), test_plan_missing=True)
        models.KCIDBTest.objects.update(status='P', last_triaged_at=timezone.now())

        checkouts_ready = models.KCIDBCheckout.objects.exclude(
            kcidbbuild__test_plan_missing=True
        )
        self.assertNotEqual(0, checkouts_ready.count())

        self.assertQuerysetEqual(
            checkouts_ready,
            models.KCIDBCheckout.objects.filter_ready_to_report()
        )

    def test_failed_build(self):
        """There are builds with test_plan_missing=True but some builds failed."""
        models.KCIDBCheckout.objects.update(valid=True, last_triaged_at=timezone.now())
        models.KCIDBBuild.objects.update(valid=True, last_triaged_at=timezone.now(), test_plan_missing=True)
        models.KCIDBTest.objects.update(status='P', last_triaged_at=timezone.now())

        failed_build = models.KCIDBBuild.objects.first()

        # test_plan_missing=True and all builds succeeded, this checkout is not ready.
        self.assertFalse(failed_build.checkout in models.KCIDBCheckout.objects.filter_ready_to_report())

        failed_build.valid = False
        failed_build.save()

        # test_plan_missing=True but a build failed, this checkout is ready.
        self.assertTrue(failed_build.checkout in models.KCIDBCheckout.objects.filter_ready_to_report())


class TestCheckoutFilterBuildSetupsFinishedToReport(utils.TestCase):
    """Unit tests for KCIDBCheckoutManager.filter_build_setups_finisihed."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
    ]

    def test_already_notification_sent_build_steps_finished(self):
        """All checkouts are tagged as notification_sent_build_setups_finished."""
        models.KCIDBCheckout.objects.update(notification_sent_build_setups_finished=True)

        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_build_setups_finished()
        )

    def test_build_setups_not_finished(self):
        """Builds without finished setup stage."""
        models.KCIDBCheckout.objects.update(notification_sent_build_setups_finished=False)
        models.KCIDBBuild.objects.update(kpet_tree_name=None)

        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_build_setups_finished()
        )

    def test_build_setups_finished_and_message_has_not_been_sent(self):
        """Build setups finished but the message has not been sent."""
        models.KCIDBCheckout.objects.update(notification_sent_build_setups_finished=False)
        models.KCIDBBuild.objects.update(kpet_tree_name='kpet_tree_name')

        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.filter_has_builds(),
            models.KCIDBCheckout.objects.filter_build_setups_finished()
        )

    def test_build_setups_finished_and_message_has_been_sent(self):
        """Build setups finished and the message has been sent."""
        models.KCIDBCheckout.objects.update(notification_sent_build_setups_finished=True)
        models.KCIDBBuild.objects.update(kpet_tree_name='kpet_tree_name')

        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_build_setups_finished()
        )


class TestCheckoutFilterTestsFinishedToReport(utils.TestCase):
    """Unit tests for KCIDBCheckoutManager.filter_tests_finisihed."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
    ]

    def test_already_notification_sent_tests_finished(self):
        """All checkouts are tagged as notification_sent_tests_finished."""
        models.KCIDBCheckout.objects.update(notification_sent_tests_finished=True)

        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_tests_finished()
        )

    def test_tests_not_finished(self):
        """Checkouts with tests unfinished."""
        models.KCIDBCheckout.objects.update(notification_sent_tests_finished=False)
        models.KCIDBTest.objects.update(status=None)

        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_tests_finished()
        )

    def test_tessts_finished_and_message_has_not_been_sent(self):
        """Tests finished but the message has not been sent."""
        models.KCIDBCheckout.objects.update(notification_sent_tests_finished=False)
        models.KCIDBTest.objects.update(status='P')

        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.filter_has_tests(),
            models.KCIDBCheckout.objects.filter_tests_finished()
        )

    def test_tests_finished_and_message_has_been_sent(self):
        """Tests finished and the message has been sent."""
        models.KCIDBCheckout.objects.update(notification_sent_tests_finished=True)
        models.KCIDBTest.objects.update(status='P')

        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_tests_finished()
        )

    def test_tests_finished_test_plan_missing(self):
        """Tests finished when a build has test_plan_missing=True."""
        models.KCIDBTest.objects.update(status='P')

        self.assertTrue(
            models.KCIDBCheckout.objects.filter_has_tests().count() > 0
        )
        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.filter_has_tests(),
            models.KCIDBCheckout.objects.filter_tests_finished()
        )

        models.KCIDBBuild.objects.update(test_plan_missing=True)

        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_tests_finished()
        )


class TestKCIDBCheckoutManagerFilterHasBuilds(utils.TestCase):
    """Unit tests for KCIDBCheckoutManager.filter_has_builds."""

    def test_checkouts_with_builds_and_without_builds(self):
        """Checkout with any builds."""
        checkout_with_build = models.KCIDBCheckout.objects.create(
            id='redhat-1',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        models.KCIDBCheckout.objects.create(
            id='redhat-2',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        models.KCIDBBuild.objects.create(
            checkout=checkout_with_build,
            id='redhat:887318',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )

        self.assertEqual(1, models.KCIDBCheckout.objects.filter_has_builds().count())
        self.assertEqual(checkout_with_build,
                         models.KCIDBCheckout.objects.filter_has_builds()[0]
                         )

    def test_all_checkouts_without_builds(self):
        """Neither checkouts have build."""
        models.KCIDBCheckout.objects.create(
            id='redhat-2',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )

        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_has_builds()
        )

    def test_all_checkouts_with_builds(self):
        """All checkouts have builds."""
        checkout = models.KCIDBCheckout.objects.create(
            id='redhat-1',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        models.KCIDBBuild.objects.create(
            checkout=checkout,
            id='redhat:887318',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )

        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.all(),
            models.KCIDBCheckout.objects.filter_has_builds()
        )


class TestKCIDBCheckoutManagerFilterHasTests(utils.TestCase):
    """Unit tests for KCIDBCheckoutManager.filter_has_tests."""

    def test_checkouts_with_tests_and_without_tests(self):
        """Checkout with any tests."""
        checkout_with_test = models.KCIDBCheckout.objects.create(
            id='redhat-1',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        models.KCIDBCheckout.objects.create(
            id='redhat-2',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        models.KCIDBBuild.objects.create(
            checkout=checkout_with_test,
            id='redhat:887318',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
        }
        models.KCIDBTest.create_from_json(data)

        self.assertEqual(1, models.KCIDBCheckout.objects.filter_has_builds().count())
        self.assertEqual(checkout_with_test,
                         models.KCIDBCheckout.objects.filter_has_builds()[0]
                         )

    def test_all_checkouts_without_tests(self):
        """Neither checkouts have tests."""
        models.KCIDBCheckout.objects.create(
            id='redhat-2',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )

        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_has_tests()
        )

    def test_all_checkouts_with_tests(self):
        """All checkouts have tests."""
        checkout = models.KCIDBCheckout.objects.create(
            id='redhat-1',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        models.KCIDBBuild.objects.create(
            checkout=checkout,
            id='redhat:887318',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
        }
        models.KCIDBTest.create_from_json(data)

        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.all(),
            models.KCIDBCheckout.objects.filter_has_tests()
        )


class TestKCIDBObjectCompare(utils.TestCase):
    """Test the equivalence of KCIDB Objects."""

    def test_checkout(self):
        """Test checkout."""
        checkout1 = models.KCIDBCheckout.objects.create(
            id='decd6167bf4f6bec1284006d0522381b44660df3',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        checkout2 = models.KCIDBCheckout.objects.create(
            id='decd6167bf4f6bec1284006d0522381b44660df4',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        self.assertTrue(checkout1.is_equivalent_to(checkout1))
        self.assertFalse(checkout1.is_equivalent_to(checkout2))
        self.assertFalse(checkout1.is_equivalent_to('string'))

    def test_build(self):
        """Test build."""
        rev = models.KCIDBCheckout.objects.create(
            id='decd6167bf4f6bec1284006d0522381b44660df3',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        build1 = models.KCIDBBuild.objects.create(
            checkout=rev,
            id='redhat:887318',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        build2 = models.KCIDBBuild.objects.create(
            checkout=rev,
            id='redhat:887319',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )

        self.assertTrue(build1.is_equivalent_to(build1))
        self.assertFalse(build1.is_equivalent_to(build2))
        self.assertFalse(build1.is_equivalent_to('string'))

    def test_test(self):
        """Test test."""
        rev = models.KCIDBCheckout.objects.create(
            id='redhat:1',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        models.KCIDBBuild.objects.create(
            checkout=rev,
            id='redhat:887318',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
        }
        data1 = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218983',
            'origin': 'redhat',
        }

        test1 = models.KCIDBTest.create_from_json(data)
        test2 = models.KCIDBTest.create_from_json(data1)

        self.assertTrue(test1.is_equivalent_to(test1))
        self.assertFalse(test1.is_equivalent_to(test2))
        self.assertFalse(test1.is_equivalent_to('string'))


class TestKCIDBTestResultFromJson(utils.TestCase):
    """Test creation of KCIDBTestResult model."""

    @mock.patch('datawarehouse.signals.kcidb_object.send', mock.Mock())
    def setUp(self):
        """Set Up."""
        models.KCIDBCheckout.create_from_json(
            {
                'id': 'redhat:1',
                'origin': 'redhat',
            }
        )
        models.KCIDBBuild.create_from_json(
            {
                'checkout_id': 'redhat:1',
                'origin': 'redhat',
                'id': 'redhat:2',
                'architecture': 'aarch64',
            }
        )
        models.KCIDBTest.create_from_json(
            {
                'build_id': 'redhat:2',
                'origin': 'redhat',
                'id': 'redhat:3',
                'comment': 'redhat three',
            }
        )

    def test_basic(self):
        """Submit only id."""
        data = {
            'id': 'redhat:2.1',
        }
        test = models.KCIDBTest.objects.get(id='redhat:3')
        result = models.KCIDBTestResult.create_from_json(test, data)

        self.assertIsInstance(result, models.KCIDBTestResult)
        self.assertEqual(result.test, test)
        self.assertEqual(result.id, 'redhat:2.1')

    def test_full(self):
        """Submit complete result."""
        data = {
            'id': 'redhat:2.1',
            'status': 'PASS',
            'name': 'sub result for test 2',
            'output_files': [
                {'url': 'http://log.server/output.file', 'name': 'output.file'},
                {'url': 'http://log.server/output.file.2', 'name': 'output.file.2'},
            ],
        }
        test = models.KCIDBTest.objects.get(id='redhat:3')
        result = models.KCIDBTestResult.create_from_json(test, data)

        self.assertIsInstance(result, models.KCIDBTestResult)
        self.assertEqual(result.test, test)
        self.assertEqual(result.id, 'redhat:2.1')
        self.assertEqual(result.status, models.ResultEnum.PASS)
        self.assertEqual(result.name, 'sub result for test 2')
        self.assertEqual(
            [{'url': file.url, 'name': file.name} for file in result.output_files.all()],
            data['output_files']
        )

    def test_checkout_pointer(self):
        """Test testresult.checkout points to the checkout."""
        data = {
            'id': 'redhat:2.1',
        }
        test = models.KCIDBTest.objects.get(id='redhat:3')
        testresult = models.KCIDBTestResult.create_from_json(test, data)

        self.assertEqual(test.build.checkout, testresult.checkout)

    def test_re_submit_files(self):
        """Submit files multiple times."""
        data = {
            'id': 'redhat:2.1',
            'status': 'PASS',
            'name': 'sub result for test 2',
            'output_files': [
                {'url': 'http://log.server/output.file', 'name': 'output.file'},
                {'url': 'http://log.server/output.file.2', 'name': 'output.file.2'},
            ],
        }
        test = models.KCIDBTest.objects.get(id='redhat:3')
        result = models.KCIDBTestResult.create_from_json(test, data)

        self.assertEqual(
            set(result.output_files.all()),
            set(models.Artifact.objects.filter(name__in=['output.file', 'output.file.2']))
        )

        # With new files, the current ones are replaced
        data.update({
            'output_files': [
                {'url': 'http://log.server/output.file.3', 'name': 'output.file.3'},
            ],
        })

        result = models.KCIDBTestResult.create_from_json(test, data)
        self.assertEqual(
            set(result.output_files.all()),
            set(models.Artifact.objects.filter(name='output.file.3'))
        )

        # If no files are provided, the files are not removed
        del data['output_files']

        result = models.KCIDBTestResult.create_from_json(test, data)
        self.assertEqual(result.output_files.count(), 1)


class TestProvenanceComponentModel(utils.TestCase):
    """Test ProvenanceComponent objects."""

    def test_create_from_dict(self):
        """Check ProvenanceComponent objects created by create_from_json."""
        provenance = models.ProvenanceComponent.create_from_dict({
            'url': 'https://url',
            'function': 'coordinator',
            'environment': {'foo': 'bar'},
            'service_name': 'gitlab',
        })

        self.assertEqual(str(provenance), 'coordinator')
        self.assertEqual(provenance.url, 'https://url')
        self.assertEqual(provenance.function, models.ProvenanceComponentFunctionEnum.COORDINATOR)
        self.assertEqual(provenance.environment, {'foo': 'bar'})
        self.assertEqual(provenance.service_name, 'gitlab')

    def test_string_format(self):
        """Check ProvenanceComponent __str__ method."""
        test_cases = [
            ('https://gitlab.com/project/-/jobs/1234', 'gitlab', 'Gitlab Job'),
            ('https://gitlab.com/project/-/pipelines/1234', 'gitlab', 'Gitlab Pipeline'),
            ('https://beaker.foo.com/recipes/11508560', 'beaker', 'Beaker Recipe'),
            ('https://foo.bar/foo', None, 'coordinator')
        ]
        for url, service_name, expected in test_cases:
            provenance = models.ProvenanceComponent.create_from_dict({
                'url': url, 'function': 'coordinator', 'service_name': service_name
            })

            self.assertEqual(str(provenance), expected)
