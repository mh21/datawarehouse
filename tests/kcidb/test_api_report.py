"""Test the report api module."""
import datetime
import json
import os
from pathlib import Path

from django.utils import timezone
from pytz import UTC

from datawarehouse import models
from datawarehouse.api.kcidb.serializers import KCIDBCheckoutSerializer
from datawarehouse.serializers import ReportSerializer
from tests import utils


class APIReportTestCaseAnonymous(utils.KCIDBTestCase):
    # pylint: disable=too-many-instance-attributes, too-many-public-methods
    """Unit tests for the report endpoints."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/issues.yaml',
        'tests/kcidb/fixtures/base_authorization.yaml',
    ]

    anonymous = True
    groups = []

    def _ensure_test_conditions(self, method):
        """Check that all conditions for testing are optimal."""
        auth_checkouts = models.KCIDBCheckout.objects.filter(id__in=self.checkouts_authorized[method])
        no_auth_checkouts = models.KCIDBCheckout.objects.exclude(id__in=self.checkouts_authorized[method])

        checks = [
            (auth_checkouts, 'No authorized checkouts'),
            (no_auth_checkouts, 'No unauthorized checkouts'),
        ]

        for check, message in checks:
            self.assertTrue(check.exists(), f'Test conditions unmet: {message} for "{method}"')

    @staticmethod
    def load_email(email):
        """Load email from assets."""
        file_name = os.path.join(utils.ASSETS_DIR, email)
        file_content = Path(file_name).read_text()

        return {
            'content': json.loads(file_content)
        }

    def test_checkout_report_create(self):
        """Test creating a report."""
        self._ensure_test_conditions('write')
        authorized_checkouts = models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized['write']
        )

        email = self.load_email('email_complete')

        for checkout in models.KCIDBCheckout.objects.all():
            authorized = checkout in authorized_checkouts and not self.anonymous
            response_code = 201 if authorized else 404

            self.assert_authenticated_post(
                response_code, 'add_report',
                f'/api/1/kcidb/checkouts/{checkout.iid}/reports',
                json.dumps(email), content_type="application/json",
                user=self.user
            )

            # Ensure it can't access unauthorized checkouts' reports.
            if not authorized:
                continue

            report = checkout.reports.first()

            self.assertEqual(
                '\nHello,\n\nWe ran automated tests on a patchset that was proposed '
                'for merging into this\nkernel tree.',
                report.body)
            self.assertEqual(
                '=?utf-8?b?4pyF?= PASS: Re: [RHEL    PATCH 000000000 2/2] ' +
                'tpm: Revert\n\t"Lorem ipsum dolor sit amet, consectetur adipisci\'s"',
                report.subject)
            self.assertEqual(
                datetime.datetime(2020, 1, 9, 19, 33, 16, tzinfo=UTC),
                report.sent_at
            )
            self.assertEqual(
                'cki.0.5FM2O05IQP@redhat.com',
                report.msgid)

            report.addr_to.get(email='redacted-email-add@redhat.com')
            report.addr_to.get(email='redacted-email@redhat.com')
            report.addr_cc.get(email='test@redhat.com')

            # Avoid duplicated msgid
            report.delete()

    def test_checkout_report_create_duplicated(self):
        """Test creating a report two times."""
        self._ensure_test_conditions('write')
        authorized_checkouts = models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized['write']
        )

        email = self.load_email('email_complete')

        for checkout in models.KCIDBCheckout.objects.all():
            authorized = checkout in authorized_checkouts and not self.anonymous
            response_code = 201 if authorized else 404

            # First time created.
            self.assert_authenticated_post(
                response_code, 'add_report',
                f'/api/1/kcidb/checkouts/{checkout.iid}/reports',
                json.dumps(email), content_type="application/json",
                user=self.user
            )

            # Ensure it can't access unauthorized checkouts' reports.
            if not authorized:
                continue

            self.assertEqual(1, checkout.reports.count())

            # Second time not created.
            response = self.assert_authenticated_post(
                400, 'add_report',
                f'/api/1/kcidb/checkouts/{checkout.iid}/reports',
                json.dumps(email), content_type="application/json")
            self.assertEqual(b'"MsgID already exists."', response.content)
            self.assertEqual(1, checkout.reports.count())

            # Avoid duplicated msgid
            checkout.reports.get().delete()

    def test_checkout_report_list(self):
        """Test getting checkout's reports."""
        self._ensure_test_conditions('read')
        authorized_checkouts = models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized['read']
        )

        for checkout in models.KCIDBCheckout.objects.all():
            checkout.reports.create(
                body='content', subject='subject', sent_at=timezone.now(), msgid=f'{checkout.iid}@redhat.com',
            )
            checkout.reports.create(
                body='content', subject='subject', sent_at=timezone.now(), msgid=f'{checkout.iid}_2@redhat.com',
            )

            response = self.client.get(f'/api/1/kcidb/checkouts/{checkout.iid}/reports')

            # Ensure it can't access unauthorized checkouts' reports.
            if checkout not in authorized_checkouts:
                self.assertEqual(404, response.status_code)
                continue

            self.assertEqual(
                response.json()['results'],
                ReportSerializer(checkout.reports.all(), many=True).data,
            )

    def test_checkout_report_get(self):
        """Test getting checkout's reports."""
        self._ensure_test_conditions('read')
        authorized_checkouts = models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized['read']
        )
        for checkout in models.KCIDBCheckout.objects.all():
            report = checkout.reports.create(
                body='content',
                subject='subject',
                sent_at=timezone.now(),
                msgid=f'{checkout.iid}@redhat.com',
            )

            response = self.client.get(f'/api/1/kcidb/checkouts/{checkout.iid}/reports/{report.id}')

            # Ensure it can't access unauthorized checkouts' reports.
            if checkout not in authorized_checkouts:
                self.assertEqual(404, response.status_code)
                continue

            self.assertEqual(
                response.json(),
                ReportSerializer(checkout.reports.get(id=report.id)).data,
            )

    def test_checkout_report_get_by_msgid(self):
        """Test getting checkout's reports by msgid"""
        self._ensure_test_conditions('read')
        authorized_checkouts = models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized['read']
        )
        for checkout in models.KCIDBCheckout.objects.all():
            report = checkout.reports.create(
                body='content',
                subject='subject',
                sent_at=timezone.now(),
                msgid=f'{checkout.iid}@redhat.com',
            )

            response = self.client.get(f'/api/1/kcidb/checkouts/{checkout.iid}/reports/{report.msgid}')
            response_2 = self.client.get(f'/api/1/kcidb/checkouts/{checkout.iid}/reports/<{report.msgid}>')

            # Ensure it can't access unauthorized checkouts' reports.
            if checkout not in authorized_checkouts:
                self.assertEqual(404, response.status_code)
                continue

            self.assertEqual(response.json(), response_2.json())
            self.assertEqual(
                response.json(),
                ReportSerializer(checkout.reports.get(id=report.id)).data,
            )

    def test_checkout_report_get_by_checkout_id(self):
        """Test getting checkout's reports by checkout id instead of iid."""
        self._ensure_test_conditions('read')
        authorized_checkouts = models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized['read']
        )
        for checkout in models.KCIDBCheckout.objects.all():
            report = checkout.reports.create(
                body='content',
                subject='subject',
                sent_at=timezone.now(),
                msgid=f'{checkout.iid}@redhat.com',
            )

            response = self.client.get(f'/api/1/kcidb/checkouts/{checkout.id}/reports/{report.id}')

            # Ensure it can't access unauthorized checkouts' reports.
            if checkout not in authorized_checkouts:
                self.assertEqual(404, response.status_code)
                continue

            self.assertEqual(
                response.json(),
                ReportSerializer(checkout.reports.get(id=report.id)).data,
            )

    def test_checkout_report_create_emails_empty(self):
        """Test creating a report with empty email fields."""
        self._ensure_test_conditions('write')
        authorized_checkouts = models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized['write']
        )

        email = self.load_email('email_no_recipients')

        for checkout in models.KCIDBCheckout.objects.all():
            authorized = checkout in authorized_checkouts and not self.anonymous
            response_code = 201 if authorized else 404

            self.assert_authenticated_post(
                response_code, 'add_report',
                f'/api/1/kcidb/checkouts/{checkout.iid}/reports',
                json.dumps(email), content_type="application/json",
                user=self.user
            )

            # Ensure it can't access unauthorized checkouts' reports.
            if not authorized:
                continue

            report = checkout.reports.get()

            self.assertEqual(0, report.addr_to.count())
            self.assertEqual(0, report.addr_cc.count())

            # Avoid duplicated msgid
            report.delete()

    def test_checkout_report_create_encoded(self):
        """Test creating report with body encoded."""
        self._ensure_test_conditions('write')
        authorized_checkouts = models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized['write']
        )

        email = self.load_email('email_b64_encoded')

        for checkout in models.KCIDBCheckout.objects.all():
            authorized = checkout in authorized_checkouts and not self.anonymous
            response_code = 201 if authorized else 404

            self.assert_authenticated_post(
                response_code, 'add_report',
                f'/api/1/kcidb/checkouts/{checkout.iid}/reports',
                json.dumps(email), content_type="application/json",
                user=self.user
            )

            # Ensure it can't access unauthorized checkouts' reports.
            if not authorized:
                continue

            report = checkout.reports.get()

            self.assertEqual(
                '\nHello,\n\nW',
                report.body[:10])

            # Avoid duplicated msgid
            report.delete()

    def test_checkout_report_create_multipart(self):
        """Test creating report from multipart email."""
        self._ensure_test_conditions('write')
        authorized_checkouts = models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized['write']
        )

        email = self.load_email('email_multipart')

        for checkout in models.KCIDBCheckout.objects.all():
            authorized = checkout in authorized_checkouts and not self.anonymous
            response_code = 201 if authorized else 404

            self.assert_authenticated_post(
                response_code, 'add_report',
                f'/api/1/kcidb/checkouts/{checkout.iid}/reports',
                json.dumps(email), content_type="application/json",
                user=self.user
            )

            # Ensure it can't access unauthorized checkouts' reports.
            if not authorized:
                continue

            report = checkout.reports.first()

            self.assertEqual(
                '\nHello,\n\nW',
                report.body[:10])

            # Avoid duplicated msgid
            report.delete()

    def test_checkout_missing_report(self):
        """Test getting missing reports."""
        self._ensure_test_conditions('read')
        authorized_checkouts = models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized['read']
        )
        models.KCIDBCheckout.objects.all().update(
            # Make them all older than one day
            start_time=timezone.now() - datetime.timedelta(hours=25)
        )

        for checkout in models.KCIDBCheckout.objects.all():
            authorized = checkout in authorized_checkouts

            # No reports, check if it's on the list of missing
            response = self.client.get('/api/1/kcidb/checkouts/-/reports/missing')

            # If it's authorized, this checkout should be in the list of missing.
            self.assertEqual(authorized, any(r['id'] == checkout.id for r in response.json()['results']))

            # Next check will be after adding a report, not applicable for unauthorized checkouts.
            if not authorized:
                continue

            checkout.reports.create(
                body='content', subject='subject', sent_at=timezone.now(),
                msgid=f'something_{checkout.iid}@redhat.com',
            )

            response = self.client.get('/api/1/kcidb/checkouts/-/reports/missing')

            self.assertFalse(any(r['id'] == checkout.id for r in response.json()['results']))
            self.assertListEqual(
                response.json()['results'],
                KCIDBCheckoutSerializer(
                    authorized_checkouts.exclude(id=checkout.id),
                    many=True
                ).data
            )

            checkout.reports.get().delete()

    def test_checkout_missing_report_since(self):
        """Test getting missing reports with since filter."""
        self._ensure_test_conditions('read')
        authorized_checkouts = models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized['read']
        )
        since = datetime.datetime.now() - datetime.timedelta(hours=48)

        models.KCIDBCheckout.objects.all().update(
            # Make them all older than one day
            start_time=timezone.now() - datetime.timedelta(hours=25)
        )
        models.KCIDBCheckout.objects.filter(
            id='public_checkout_invalid'
        ).update(
            # Move it outside the 'since' filter
            start_time=timezone.now() - datetime.timedelta(hours=48+1)
        )

        response = self.client.get(f'/api/1/kcidb/checkouts/-/reports/missing?since={since}')

        self.assertEqual(
            response.json()['results'],
            KCIDBCheckoutSerializer(
                authorized_checkouts.exclude(id='public_checkout_invalid'),
                many=True
            ).data
        )

    def test_checkout_missing_report_since_with_report(self):
        """Test getting missing reports with since filter."""
        self._ensure_test_conditions('read')
        authorized_checkouts = models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized['read']
        )
        since = datetime.datetime.now() - datetime.timedelta(hours=48)

        models.KCIDBCheckout.objects.all().update(
            # Make them all older than one day
            start_time=timezone.now() - datetime.timedelta(hours=25)
        )
        checkout = models.KCIDBCheckout.objects.first()
        checkout.reports.create(
            body='content', subject='subject', sent_at=timezone.now(), msgid='<someting@redhat.com>',
        )

        response = self.client.get(f'/api/1/kcidb/checkouts/-/reports/missing?since={since}')
        self.assertEqual(
            response.json()['results'],
            KCIDBCheckoutSerializer(
                authorized_checkouts.exclude(id=checkout.id),
                many=True
            ).data
        )

        checkout.reports.get().delete()

    def test_report_get(self):
        """Test getting reports."""
        self._ensure_test_conditions('read')
        authorized_checkouts = models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized['read']
        )
        for checkout in models.KCIDBCheckout.objects.all():
            report = checkout.reports.create(
                body='content',
                subject='subject',
                sent_at=timezone.now(),
                msgid=f'{checkout.iid}@redhat.com',
            )

        for report in models.Report.objects.all():
            response_id = self.client.get(f'/api/1/kcidb/reports/{report.id}')
            response_msgid = self.client.get(f'/api/1/kcidb/reports/{report.msgid}')
            response_msgid_gtlt = self.client.get(f'/api/1/kcidb/reports/<{report.msgid}>')

            # Ensure it can't access unauthorized checkouts' reports.
            if report.kcidb_checkout not in authorized_checkouts:
                self.assertEqual(404, response_id.status_code)
                continue

            self.assertEqual(response_id.json(), response_msgid.json())
            self.assertEqual(response_id.json(), response_msgid_gtlt.json())
            self.assertEqual(response_id.json(), ReportSerializer(report).data)

    def test_report_get_checkout(self):
        """Test getting checkouts for a report."""
        self._ensure_test_conditions('read')
        authorized_checkouts = models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized['read']
        )
        for checkout in models.KCIDBCheckout.objects.all():
            report = checkout.reports.create(
                body='content',
                subject='subject',
                sent_at=timezone.now(),
                msgid=f'{checkout.iid}@redhat.com',
            )

        for report in models.Report.objects.all():
            response_id = self.client.get(f'/api/1/kcidb/reports/{report.id}/checkout')
            response_msgid = self.client.get(f'/api/1/kcidb/reports/{report.msgid}/checkout')
            response_msgid_gtlt = self.client.get(f'/api/1/kcidb/reports/<{report.msgid}>/checkout')

            # Ensure it can't access unauthorized checkouts' reports.
            if report.kcidb_checkout not in authorized_checkouts:
                self.assertEqual(404, response_id.status_code)
                continue

            self.assertEqual(response_id.json(), response_msgid.json())
            self.assertEqual(response_id.json(), response_msgid_gtlt.json())
            self.assertEqual(response_id.json(), KCIDBCheckoutSerializer(report.kcidb_checkout).data)


class APIReportTestCaseNoGroup(APIReportTestCaseAnonymous):
    """APIReportTestCase with no groups assigned."""

    anonymous = False
    groups = []


class APIReportTestCaseReadGroups(APIReportTestCaseAnonymous):
    """APIReportTestCase with a group with read authorization."""

    anonymous = False
    groups = ['group_a']


class APIReportTestCaseWriteGroups(APIReportTestCaseAnonymous):
    """APIReportTestCase with a group with write authorization."""

    anonymous = False
    groups = ['group_b']


class APIReportTestCaseAllGroups(APIReportTestCaseAnonymous):
    """APIReportTestCase with all groups."""

    anonymous = False
    groups = ['group_a', 'group_b']
