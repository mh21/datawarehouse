# Git Tree authorization

**WIP** This document and the implementation are still a work in progress.

Datawarehouse implements an object-level permissions schema to limit the read
and write access of users to certain git trees and it's related objects.

This is designed to allow managing read and write permissions per tree.
Some of the requirements needed by this implementation are the following:

- Allow anonymous users access to certain trees.
- Allow giving read and/or write permissions to a certain subset of trees to a
  logged in user.

## Design

### GitTree

The `GitTree` object represents a certain tree, and it's related to a
`GitTreePolicy`.

### GitTreePolicy

This model contains the information about the rules to access the `GitTree`
that are linked to it.
`GitTreePolicy` contains a name to represent the policy, and both `read_group`
and `write_group`, linked to the `Group` object from Django's authentication
framework.

### How it is calculated

There are certain cases considered for this design.

1. `GitTree` has no `GitTreePolicy`.

If a tree has no policy linked, it means that it is private.
No one has access to read or write it.
This is the default scenario for new trees.

1. `GitTree` has `GitTreePolicy`, but `{method}_group` is null.

If a policy contains one of the group values (`read_group` or `write_group`)
null, it means that that operation does not need special authorization.
For example, a public readable tree will have a policy with null `read_group`,
allowing to be read by anyone.

1. `GitTree` has `GitTreePolicy`.

If the policy linked to a tree has a group linked on `read_group` or
`write_group`, it means that the user will need to be on that group to be
authorized for the operation.

### Queries and authorization

All queries need to take authorization into consideration.
This means that all `filter` and `get` calls need to return only the results
allowed to the user performing the query.

### Authorization caching

When a user is logged in, it's authorization information is loaded into the
session data.
This means that for an authorization change to be reflected on the user, it
will need to finish the session and log in again.

## Authorizing users

To give a user access to a certain tree, it's necessary to add it to the
corresponding group.

TODO: define groups naming and documentation
