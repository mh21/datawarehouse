# KCIDB - Checkout

## Get

Get a single checkout.

`GET /api/1/kcidb/checkouts/$checkout_id`

| Name | Type | Required | Description |
|------|------|----------|-------------|
| `checkout_id` | `str/int` | Yes | `id` or `iid` of the checkout to get. |

Example of response:

```json
{
    "id": "b8fba93561c984f336d47d6d544be3a2a920bac3",
    "origin": "redhat",
    "tree_name": "upstream",
    "git_repository_url": "https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable-rc.git",
    "git_repository_branch": "queue/5.7",
    "git_commit_hash": "b8fba93561c984f336d47d6d544be3a2a920bac3",
    "start_time": "2020-06-30T17:06:26.357000Z",
    "valid": true,
    "misc": {
        "iid": 137
    }
}
```

## List

Get a list of checkouts.

`GET /api/1/kcidb/checkouts`

Example of response:

```json
{
    "count": 168,
    "next": "http://server/api/1/kcidb/checkouts?limit=30&offset=30",
    "previous": null,
    "results": [
        {
            "id": "b8fba93561c984f336d47d6d544be3a2a920bac3",
            "origin": "redhat",
            "tree_name": "upstream",
            "git_repository_url": "https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable-rc.git",
            "git_repository_branch": "queue/5.7",
            "git_commit_hash": "b8fba93561c984f336d47d6d544be3a2a920bac3",
            "start_time": "2020-06-30T17:06:26.357000Z",
            "valid": true,
            "misc": {
                "iid": 137
            }
        },
        ...
    ]
}
```

## Get Recipients

Get report recipients for a checkout.

`GET /api/1/kcidb/checkouts/$checkout_id/recipients`

| Name | Type | Required | Description |
|------|------|----------|-------------|
| `checkout_id` | `str/int` | Yes | `id` or `iid` of the checkout to get. |

Example of response:

```json
{
    "send_to": [
        "to@email.com"
    ],
    "send_cc": [
        "cc@email.com"
    ],
    "send_bcc": [
        "bcc@email.com"
    ]
}
```
