"""Apps."""
from django.apps import AppConfig


class DatawarehouseConfig(AppConfig):
    """APP config class."""

    name = 'datawarehouse'

    def ready(self):
        """Ready function."""
        # pylint: disable=unused-import,import-outside-toplevel
        import datawarehouse.signal_receivers  # noqa
