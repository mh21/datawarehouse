"""KCIDB Views."""
import datetime

from cki_lib.logger import get_logger
from cki_lib.misc import strtobool
from django.core.exceptions import PermissionDenied
from django.db.models import Q
from django.http import Http404
from django.http import HttpResponse
from django.http import HttpResponseBadRequest
from django.http import HttpResponseRedirect
from django.template import loader
from django.urls import reverse
from django.utils import timezone

from . import authorization
from . import models
from . import pagination
from . import utils

LOGGER = get_logger(__name__)


def checkouts_list(request):
    """Get list of checkouts."""
    template = loader.get_template('web/kcidb/checkouts.html')
    page = request.GET.get('page')

    checkouts = models.KCIDBCheckout.objects.filter_authorized(request)
    checkouts, filters = utils.filter_checkouts_view(request, checkouts)
    git_branches = (
        checkouts
        .order_by('git_repository_branch')
        .distinct('git_repository_branch')
        .values_list('git_repository_branch', flat=True)
    )

    git_trees = (
        models.GitTree.objects
        .filter(kcidbcheckout__in=checkouts)
        .order_by('name')
        .distinct()
    )

    paginator = pagination.EndlessPaginator(
        checkouts.values_list('iid', flat=True),
        30
    )
    checkout_iids = paginator.get_page(page)

    checkouts = (
        models.KCIDBCheckout.objects
        .aggregated()
        .filter(iid__in=checkout_iids)
        .prefetch_related(
            'tree',
        )
    )

    context = {
        'checkouts': checkouts,
        'paginator': checkout_iids,
        # Filter parameters
        'gittrees': git_trees,
        'git_branches': git_branches,
        'filters': filters,
    }

    return HttpResponse(template.render(context, request))


def checkouts_list_by_failure(request, stage):
    """Show failed checkouts classified by stage."""
    template = loader.get_template('web/kcidb/checkouts_failures.html')
    page = request.GET.get('page')

    checkouts = models.KCIDBCheckout.objects.filter_authorized(request)

    objects = {
        'checkout': checkouts.filter(valid=False),
        'build': checkouts.filter(kcidbbuild__valid=False),
        'test': checkouts.filter(
            # Filter failed tests and exclude the waived ones.
            # .exclude() generated a *really* slow query. See cki-project/datawarehouse!258
            # waived__in=(False, None) doesn't handle NULL. See https://code.djangoproject.com/ticket/13579
            #
            # status in UNSUCCESSFUL_STATUSES & (waived==False | waived is NULL)
            Q(kcidbbuild__kcidbtest__status__in=models.KCIDBTest.UNSUCCESSFUL_STATUSES) &
            (
                Q(kcidbbuild__kcidbtest__waived=False) |
                Q(kcidbbuild__kcidbtest__waived__isnull=True)
            )
        )
    }

    if stage not in ['all'] + list(objects):
        return HttpResponseBadRequest(f'Not sure what {stage} is.')

    stages = list(objects) if stage == 'all' else [stage]

    # Get a list of iids of the checkouts with failures
    checkout_iids = []
    for stage_name in stages:
        checkouts, filters = utils.filter_checkouts_view(request, objects[stage_name])
        checkout_iids.extend(
            checkouts.values_list('iid', flat=True)
        )

    # Sort ids descendingly and remove duplicates
    checkout_iids = sorted(list(set(checkout_iids)), reverse=True)

    paginator = pagination.EndlessPaginator(checkout_iids, 30)
    checkout_iids_page = paginator.get_page(page)

    checkouts = (
        models.KCIDBCheckout.objects
        .aggregated()
        .filter(iid__in=checkout_iids_page)
        .prefetch_related(
            'tree',
        )
    )

    context = {
        'paginator': checkout_iids_page,
        'checkouts': checkouts,
        'stage_filter': stage,
        'stages': list(objects),
        # Filter parameters
        'gittrees': models.GitTree.objects.filter_authorized(request).order_by('name').distinct(),
        'filters': filters,
    }

    return HttpResponse(template.render(context, request))


def checkouts_baselines_get(request):
    """Get list of baselines."""
    template = loader.get_template('web/kcidb/checkouts_baselines_history.html')
    page = request.GET.get('page')

    git_repository_url = request.GET.get('git_repository_url')
    git_repository_branch = request.GET.get('git_repository_branch')
    tree_name = request.GET.get('tree_name')
    kpet_tree_name = request.GET.get('kpet_tree_name')
    package_name = request.GET.get('package_name')

    required_params = (
        (git_repository_url and git_repository_branch and tree_name) or
        (kpet_tree_name and package_name)
    )
    if not required_params:
        return HttpResponseRedirect(reverse('views.kcidb.baselines'))

    checkouts_iids = (
        models.KCIDBCheckout.objects
        .filter_authorized(request)
        .filter(
            **utils.clean_dict(
                {
                    'related_merge_request__isnull': True,
                    'git_repository_url': git_repository_url,
                    'git_repository_branch': git_repository_branch,
                    'tree__name': tree_name,
                    'kcidbbuild__kpet_tree_name': kpet_tree_name,
                    'kcidbbuild__package_name': package_name,
                }
            )
        )
        .values_list('iid', flat=True)
    )

    paginator = pagination.EndlessPaginator(checkouts_iids, 30)
    checkouts_iids_page = paginator.get_page(page)

    checkouts = (
        models.KCIDBCheckout.objects
        .annotated_by_architecture()
        .filter(iid__in=checkouts_iids_page)
        .order_by('-iid')
        .select_related(
            'tree',
        )
    )

    context = {
        'checkouts': checkouts,
        'architectures': models.ArchitectureEnum,
        'paginator': checkouts_iids_page,
        'git_repository_url': git_repository_url,
        'git_repository_branch': git_repository_branch,
        'tree_name': tree_name,
        'kpet_tree_name': kpet_tree_name,
        'package_name': package_name,
    }

    return HttpResponse(template.render(context, request))


def checkouts_baselines_list(request):
    """Get list of baselines."""
    template = loader.get_template('web/kcidb/checkouts_baselines.html')

    last_2_months = timezone.now() - datetime.timedelta(days=60)
    checkouts_iids = (
        models.KCIDBCheckout.objects
        .filter_authorized(request)
        .filter(
            start_time__gte=last_2_months,
            scratch=False,
        )
        .exclude(git_repository_url=None)  # Remove checkouts without necessary data
        .order_by('git_repository_url', 'git_repository_branch', 'tree__name', '-iid')
        .distinct('git_repository_url', 'git_repository_branch', 'tree__name')
        .values_list('iid', flat=True)
    )

    # Include Checkouts that have no git_repository_url grouping them by kpet_tree_name
    # and package_name. This is intended to include Brew official builds.
    no_git_url_checkouts = (
        models.KCIDBCheckout.objects
        .filter_authorized(request)
        .filter(
            start_time__gte=last_2_months,
            scratch=False,
        )
        .order_by('kcidbbuild__kpet_tree_name', 'kcidbbuild__package_name', '-iid')
        .distinct('kcidbbuild__kpet_tree_name', 'kcidbbuild__package_name')
        .values_list('iid', flat=True)
    )

    checkouts = (
        models.KCIDBCheckout.objects
        .annotated_by_architecture()
        .filter(iid__in=list(checkouts_iids) + list(no_git_url_checkouts))
        .order_by('git_repository_url', 'git_repository_branch', 'kcidbbuild__kpet_tree_name', 'tree__name')
        .prefetch_related('kcidbbuild_set')
        .distinct()
        .select_related('tree')
    )

    context = {
        'checkouts': checkouts,
        'architectures': models.ArchitectureEnum,
    }

    return HttpResponse(template.render(context, request))


def checkouts_get(request, checkout_id):
    """Get a single checkout."""
    template = loader.get_template('web/kcidb/checkout.html')

    # First filter authorized checkout and then run final query as it's not
    # possible to chain filter_authorized() and aggregated().
    checkout, redirect_request = utils.get_object_or_404_or_redirect_url(
        request,
        models.KCIDBCheckout.objects.filter_authorized(request),
        checkout_id
    )
    if redirect_request is not None:
        return redirect_request

    checkout = (
        models.KCIDBCheckout.objects.aggregated()
        .filter(iid=checkout.iid)
        .select_related(
            'log',
            'origin',
            'tree',
        )
        .prefetch_related(
            'patches',
            'kcidbbuild_set',
            'kcidbbuild_set__kcidbtest_set',
            'kcidbbuild_set__kcidbtest_set__test',
        )
    ).get()

    tests = (
        models.KCIDBTest.objects
        .filter(
            build__checkout=checkout
        )
        .select_related(
            'build',
            'test',
        )
        .prefetch_related(
            'issues',
        )
    )

    builds = (
        models.KCIDBBuild.objects
        .aggregated()
        .filter(
            checkout=checkout
        )
        .prefetch_related(
            'kcidbtest_set',
            'kcidbtest_set__test',
        )
    )

    issues = (
        models.Issue.objects
        .filter(
            resolved_at__isnull=True
        )
        .select_related(
            'kind'
        )
    )

    grouped_issues = utils.group_issue_occurrences(
        models.IssueOccurrence.objects.filter(
            Q(kcidb_checkout=checkout) |
            Q(kcidb_build__in=builds) |
            Q(kcidb_test__in=tests)
        )
    )

    context = {
        'builds': builds,
        'builds_failed': builds.exclude(valid=True),
        'issues': issues,
        'grouped_issues': grouped_issues,
        'checkout': checkout,
        'checkouts_failed': [checkout] if not checkout.valid else [],
        'tests': tests,
        'tests_failed': tests.exclude(status=models.ResultEnum.PASS),
    }

    return HttpResponse(template.render(context, request))


def builds_list(request):
    """Get list of builds."""
    template = loader.get_template('web/kcidb/builds.html')
    page = request.GET.get('page')

    builds = models.KCIDBBuild.objects.filter_authorized(request)
    builds, filters = utils.filter_checkouts_view(request, builds, path_to_checkout='checkout__')
    builds, filters_build = utils.filter_builds_view(request, builds)

    filters.update(filters_build)

    paginator = pagination.EndlessPaginator(
        builds.values_list('iid', flat=True),
        30
    )
    build_iids = paginator.get_page(page)

    builds = (
        models.KCIDBBuild.objects
        .aggregated()
        .filter(iid__in=build_iids)
        .prefetch_related(
            'checkout',
            'checkout__tree',
        )
    )

    package_names = (
        models.KCIDBBuild.objects
        .filter_authorized(request)
        .order_by('package_name')
        .distinct()
        .values_list('package_name', flat=True)
    )

    context = {
        'builds': builds,
        'paginator': build_iids,
        # Filter parameters
        'architectures': models.ArchitectureEnum,
        'package_names': package_names,
        'gittrees': models.GitTree.objects.filter_authorized(request).order_by('name').distinct(),
        'filters': filters,
    }

    return HttpResponse(template.render(context, request))


def builds_get(request, build_id):
    """Get a single build."""
    template = loader.get_template('web/kcidb/build.html')

    # First filter authorized build and then run final query as it's not
    # possible to chain filter_authorized() and aggregated().
    build, redirect_request = utils.get_object_or_404_or_redirect_url(
        request,
        models.KCIDBBuild.objects.filter_authorized(request),
        build_id
    )
    if redirect_request is not None:
        return redirect_request

    build = (
        models.KCIDBBuild.objects.aggregated()
        .filter(iid=build.iid)
        .select_related(
            'compiler',
            'log',
            'origin',
            'checkout',
        )
        .prefetch_related(
            'input_files',
            'output_files',
            'kcidbtest_set',
            'kcidbtest_set__output_files',
            'kcidbtest_set__test',
        )
    ).get()

    tests = (
        models.KCIDBTest.objects
        .filter(
            build=build
        )
        .select_related(
            'build',
            'test',
        )
        .prefetch_related(
            'issues',
        )
    )

    issues = (
        models.Issue.objects
        .filter(
            resolved_at__isnull=True
        )
        .select_related(
            'kind'
        )
    )

    grouped_issues = utils.group_issue_occurrences(
        models.IssueOccurrence.objects.filter(
            Q(kcidb_build=build) |
            Q(kcidb_test__in=tests)
        )
    )

    context = {
        'build': build,
        'builds_failed': [build] if not build.valid else [],
        'issues': issues,
        'grouped_issues': grouped_issues,
        'tests': tests,
        'tests_failed': tests.exclude(status=models.ResultEnum.PASS),
    }

    return HttpResponse(template.render(context, request))


def tests_list(request):  # pylint: disable=too-many-locals
    """Get list of tests."""
    template = loader.get_template('web/kcidb/tests.html')
    page = request.GET.get('page')

    result_filter = request.GET.get('result_filter')
    arch_filter = request.GET.get('arch_filter')
    issues_tagged_filter = request.GET.get('issues_tagged_filter')
    test_filter = request.GET.get('test_filter')
    host_filter = request.GET.get('host_filter')
    tree_filter = request.GET.get('tree_filter')
    kernel_version_filter = request.GET.get('kernel_version_filter')
    sort_by_start_time = request.GET.get('sort_by_start_time')

    tests = models.KCIDBTest.objects.filter_authorized(request)

    filters = utils.clean_dict({
        'status': getattr(models.ResultEnum, result_filter) if result_filter else None,
        'build__architecture': getattr(models.ArchitectureEnum, arch_filter) if arch_filter else None,
        'issues__isnull': not strtobool(issues_tagged_filter) if issues_tagged_filter else None,
        'test__name__regex': test_filter or None,
        'environment__fqdn__regex': host_filter or None,
        'build__checkout__tree__name__regex': tree_filter or None,
        'build__checkout__kernel_version__regex': kernel_version_filter or None,
        'start_time__isnull': False if sort_by_start_time else None,
    })

    tests = tests.filter(**filters).order_by(
                '-start_time' if sort_by_start_time else '-iid'
            )

    paginator = pagination.EndlessPaginator(
        tests.values_list('iid', flat=True),
        30
    )

    test_iids = paginator.get_page(page)

    tests = (
        models.KCIDBTest.objects
        .filter(iid__in=test_iids)
        .prefetch_related(
            'build',
            'build__checkout__tree',
        ).order_by(
            '-start_time' if sort_by_start_time else '-iid'
        )
    )

    context = {
        'tests': tests,
        'paginator': test_iids,
        # Filter parameters
        'result_filter': result_filter,
        'arch_filter': arch_filter,
        'issues_tagged_filter': issues_tagged_filter,
        'test_filter': test_filter,
        'host_filter': host_filter,
        'tree_filter': tree_filter,
        'kernel_version_filter': kernel_version_filter,
        'sort_by_start_time': sort_by_start_time,
    }

    return HttpResponse(template.render(context, request))


def tests_get(request, test_id):
    """Get a single test."""
    template = loader.get_template('web/kcidb/test.html')

    test = (
        models.KCIDBTest.objects
        .filter_authorized(request)
        .select_related(
            'build',
            'build__checkout',
            'environment',
            'origin',
            'test',
        )
        .prefetch_related(
            'output_files',
        )
    )

    test, redirect_request = utils.get_object_or_404_or_redirect_url(
        request,
        test,
        test_id
    )
    if redirect_request is not None:
        return redirect_request

    issues = (
        models.Issue.objects
        .filter(
            resolved_at__isnull=True
        )
        .select_related(
            'kind'
        )
    )

    grouped_issues = utils.group_issue_occurrences(
        models.IssueOccurrence.objects.filter(
            Q(kcidb_test=test)
        )
    )

    results = (
        test.kcidbtestresult_set.all()
        .prefetch_related(
            'output_files',
            'issues',
        )
    )

    context = {
        'test': test,
        'tests_failed': [test] if not test.status or test.status != models.ResultEnum.PASS else [],
        'issues': issues,
        'grouped_issues': grouped_issues,
        'results': results,
    }

    return HttpResponse(template.render(context, request))


def kcidb_issue(request):
    # pylint: disable=too-many-nested-blocks
    """Link/Unlink checkouts, builds or tests to a given issue."""
    if request.method == "POST":
        objects = {
            'checkout': {
                'permission': 'datawarehouse.change_kcidbcheckout',
                'elements': models.KCIDBCheckout.objects.filter(
                    iid__in=request.POST.getlist('checkout_iids')
                ),
            },
            'build': {
                'permission': 'datawarehouse.change_kcidbbuild',
                'elements': models.KCIDBBuild.objects.filter(
                    iid__in=request.POST.getlist('build_iids')
                ),
            },
            'test': {
                'permission': 'datawarehouse.change_kcidbtest',
                'elements': models.KCIDBTest.objects.filter(
                    iid__in=request.POST.getlist('test_iids')
                ),
            },
        }

        # Check all permissions before performing any change.
        for obj in objects.values():
            if obj['elements'] and not request.user.has_perm(obj['permission']):
                raise PermissionDenied()

            all_objects_authorized = authorization.PolicyAuthorizationBackend.all_objects_authorized(
                request,
                obj['elements'],
            )
            if not all_objects_authorized:
                raise Http404()

        issue_id = request.POST.get('issue_id')
        issue = models.Issue.objects.get(id=issue_id)

        action = request.POST.get('action', 'add')
        for obj in objects.values():
            for element in obj['elements']:
                if action == 'add':
                    if isinstance(element, models.KCIDBTest) and request.POST.get(f'test_{element.iid}_result_iids'):
                        # Create the IssueOccurrence on the KCIDBTestResult directly.
                        test_results = models.KCIDBTestResult.objects.filter(
                            iid__in=request.POST.getlist(f'test_{element.iid}_result_iids')
                        )
                        for test_result in test_results:
                            utils.create_issue_occurrence(issue, test_result, request)
                    else:
                        utils.create_issue_occurrence(issue, element, request)
                elif action == 'remove':
                    element.issues.remove(issue)
                else:
                    HttpResponseBadRequest(f'Action {action} unknown.')

                LOGGER.info('action="%s issue on %s" user="%s" issue_id="%s" iid="%s"',
                            action, element.__class__.__name__, request.user.username, issue.id, element.iid)

    return HttpResponseRedirect(request.POST.get('redirect_to'))


def search(request):
    """Search for checkouts by id or by pipeline_id."""
    template = loader.get_template('web/search.html')
    page = request.GET.get('page')
    query = request.GET.get('q', '').strip()

    if not query:
        return HttpResponse(template.render({}, request))

    query_filter = (
        Q(id__icontains=query) |
        Q(kernel_version__icontains=query)
    )

    if query.isdigit():
        query_filter |= Q(iid=query)

    checkouts = (
        models.KCIDBCheckout.objects
        .filter_authorized(request)
        .filter(query_filter)
    )

    paginator = pagination.EndlessPaginator(
        checkouts.values_list('iid', flat=True),
        30
    )
    checkout_iids = paginator.get_page(page)

    checkouts = (
        models.KCIDBCheckout.objects
        .aggregated()
        .filter(iid__in=checkout_iids)
        .prefetch_related(
            'tree',
        )
    )

    context = {
        'checkouts': checkouts,
        'paginator': checkout_iids,
        'query': query,
    }

    return HttpResponse(template.render(context, request))


def revision_redirect(request, revision_iid):
    """
    Resolve revision urls into checkouts.

    Keep /kcidb/revisions/{iid} compatibility for old links.
    """
    return HttpResponseRedirect(
        reverse('views.kcidb.checkouts', args=[revision_iid])
    )
