"""Cron tasks."""
from datawarehouse.cron.jobs import BuildSetupsFinishedCheckouts
from datawarehouse.cron.jobs import ClearExpiredSessions
from datawarehouse.cron.jobs import DeleteExpiredArtifacts
from datawarehouse.cron.jobs import DeleteOldNotConfirmedUsers
from datawarehouse.cron.jobs import ReadyToReportCheckouts
from datawarehouse.cron.jobs import RemoveExpiredCaptachas
from datawarehouse.cron.jobs import RunQueuedTasks
from datawarehouse.cron.jobs import SendAccountDeletionWarning
from datawarehouse.cron.jobs import SendQueuedEmails
from datawarehouse.cron.jobs import SendQueuedMessages
from datawarehouse.cron.jobs import TestsFinishedCheckouts
from datawarehouse.cron.jobs import UpdateLdapGroupMembers
from datawarehouse.cron.metrics import UpdatePrometheusMetrics1h
from datawarehouse.cron.metrics import UpdatePrometheusMetrics5m

JOBS = [
    ClearExpiredSessions(),
    DeleteExpiredArtifacts(),
    DeleteOldNotConfirmedUsers(),
    ReadyToReportCheckouts(),
    BuildSetupsFinishedCheckouts(),
    TestsFinishedCheckouts(),
    RemoveExpiredCaptachas(),
    RunQueuedTasks(),
    SendAccountDeletionWarning(),
    SendQueuedEmails(),
    SendQueuedMessages(),
    UpdateLdapGroupMembers(),
    UpdatePrometheusMetrics5m(),
    UpdatePrometheusMetrics1h(),
]
