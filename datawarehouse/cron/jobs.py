"""Definition of cron jobs."""
from captcha.models import CaptchaStore
from cki_lib.logger import get_logger
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core import management
from django.db import transaction
from django.template import loader
from django.utils import timezone

from datawarehouse import metrics
from datawarehouse import models
from datawarehouse import scripts
from datawarehouse import signals
from datawarehouse import utils
from datawarehouse.cron.utils import CronJobWithDBConnection
from datawarehouse.utils import notify_user

LOGGER = get_logger(__name__)


class DeleteExpiredArtifacts(CronJobWithDBConnection):
    """Delete expired artifacts."""

    schedule = '0 */1 * * *'

    def entrypoint(self, **_):
        """Run job."""
        to_delete = models.Artifact.objects.filter(expiry_date__lt=timezone.now())
        LOGGER.info("Deleting %i expired artifacts", to_delete.count())
        to_delete.delete()


class DeleteOldNotConfirmedUsers(CronJobWithDBConnection):
    """Delete users older than setup amount days that are not confirmed."""

    schedule = '0 */1 * * *'

    def entrypoint(self, **_):
        """Run job."""
        if not settings.FF_DEL_NOT_CONFIRMED_USERS_OLDER_THAN_DAYS:
            return
        old_time = timezone.now() - timezone.timedelta(days=settings.FF_DEL_NOT_CONFIRMED_USERS_OLDER_THAN_DAYS)
        users = get_user_model().objects.filter(last_login=None, date_joined__lt=old_time)
        LOGGER.info('Deleting not confirmed users: %s', ", ".join([u.username for u in users]))
        users.delete()


class SendAccountDeletionWarning(CronJobWithDBConnection):
    """Send mail to accounts that are going to be deleted."""

    schedule = '0 */1 * * *'

    def entrypoint(self, **_):
        """Run job."""
        if not settings.FF_DEL_NOT_CONFIRMED_USERS_OLDER_THAN_DAYS:
            return
        half_days = int(settings.FF_DEL_NOT_CONFIRMED_USERS_OLDER_THAN_DAYS/2)
        half_old_time = timezone.now() - timezone.timedelta(days=half_days)
        users = get_user_model().objects.filter(last_login=None,
                                                date_joined__lt=half_old_time,
                                                preferences__account_deletion_warning_sent_at=None)
        for user in users:
            user_pref, _ = models.UserPreferences.objects.get_or_create(user=user)
            email_template = loader.get_template('registration/user_delete_warning_email.html')

            email_context = {
                "user": user,
                "days": half_days
            }

            notify_user(
                subject="DataWarehouse account deletion notification",
                message=email_template.render(email_context),
                user=user
            )

            user_pref.account_deletion_warning_sent_at = timezone.now()
            user_pref.save()


class ClearExpiredSessions(CronJobWithDBConnection):
    """Clear expired sessions from database."""

    schedule = '0 */1 * * *'

    def entrypoint(self, **_):
        """Run job."""
        LOGGER.info("Clearing expired sessions.")
        management.call_command("clearsessions", verbosity=1)


class ReadyToReportCheckouts(CronJobWithDBConnection):
    """Tag KCIDBCheckouts as ready to report and broadcast them on RabbitMQ."""

    schedule = '*/10 * * * *'

    def entrypoint(self, **_):
        """Run job."""
        LOGGER.info("Checking KCIDBCheckouts ready to report.")
        checkouts = models.KCIDBCheckout.objects.filter_ready_to_report().select_for_update()
        with transaction.atomic():
            if not checkouts:
                return

            signals.kcidb_object.send(
                sender='cron.jobs.ReadyToReportCheckouts',
                status=models.ObjectStatusEnum.READY_TO_REPORT,
                object_type='checkout',
                # For some reason, passing the QuerySet does not allow testing it correctly,
                # mock always sees an empty QuerySet. Cast it to a list.
                objects=list(checkouts),
            )

            for checkout in list(checkouts):
                metrics.update_time_to_report(checkout)
                metrics.update_checkout_metrics(checkout)

            checkouts.update(ready_to_report=True)


class BuildSetupsFinishedCheckouts(CronJobWithDBConnection):
    """Tag KCIDBCheckouts as send build setups finished and broadcast them on RabbitMQ."""

    schedule = '*/10 * * * *'

    def entrypoint(self, **_):
        """Run job."""
        LOGGER.info("Checking KCIDBCheckouts with all the build setups finished.")
        checkouts = models.KCIDBCheckout.objects.filter_build_setups_finished().select_for_update()
        with transaction.atomic():
            for checkout in checkouts:
                signals.kcidb_object.send(
                    sender='cron.jobs.BuildSetupsFinishedCheckouts',
                    status=models.ObjectStatusEnum.BUILD_SETUPS_FINISHED,
                    object_type='checkout',
                    objects=[checkout],
                )

            checkouts.update(notification_sent_build_setups_finished=True)


class TestsFinishedCheckouts(CronJobWithDBConnection):
    """Tag KCIDBCheckouts as send all tests finished and broadcast them on RabbitMQ."""

    schedule = '*/10 * * * *'

    def entrypoint(self, **_):
        """Run job."""
        LOGGER.info("Checking KCIDBCheckouts with all the build setups finished.")
        checkouts = models.KCIDBCheckout.objects.filter_tests_finished().select_for_update()
        with transaction.atomic():
            for checkout in checkouts:
                signals.kcidb_object.send(
                    sender='cron.jobs.TestsFinishedCheckouts',
                    status=models.ObjectStatusEnum.TESTS_FINISHED,
                    object_type='checkout',
                    objects=[checkout],
                )

            checkouts.update(notification_sent_tests_finished=True)


class RemoveExpiredCaptachas(CronJobWithDBConnection):
    """Clear expired captchas from database."""

    schedule = '*/10 * * * *'

    def entrypoint(self, **_):
        """Run job."""
        LOGGER.info("Clearing expired captchas.")
        CaptchaStore.remove_expired()


class UpdateLdapGroupMembers(CronJobWithDBConnection):
    """Update LDAP group members."""

    schedule = '0 */1 * * *'

    def entrypoint(self, **_):
        """Run job."""
        LOGGER.info("Updating LDAP group members.")
        scripts.update_ldap_group_members()


class RunQueuedTasks(CronJobWithDBConnection):
    """Run queued tasks."""

    schedule = '*/1 * * * *'
    back_off_limit = 5

    def entrypoint(self, **_):
        """Run queued tasks."""
        LOGGER.info("Running queued tasks.")
        tasks = models.QueuedTask.objects.filter_ready_to_run().select_for_update(skip_locked=True)

        with transaction.atomic():
            for task in tasks:
                try:
                    task.run()
                except Exception:  # pylint: disable=broad-except
                    LOGGER.exception('Error running QueuedTask (%s), will be retried.', task)


class SendQueuedMessages(CronJobWithDBConnection):
    """Send queued MessagePending."""

    schedule = '*/1 * * * *'
    back_off_limit = 5

    def entrypoint(self, **_):
        """Run queued tasks."""
        if settings.RABBITMQ_SEND_ENABLED:
            LOGGER.info("Sending queued messages.")
            utils.MSG_QUEUE.send()


class SendQueuedEmails(CronJobWithDBConnection):
    """Send queued EmailPending."""

    schedule = '*/1 * * * *'
    back_off_limit = 5

    def entrypoint(self, **_):
        """Send queued emails."""
        if settings.EMAIL_SEND_ENABLED:
            LOGGER.info("Sending queued emails.")
            utils.EMAIL_QUEUE.send()
