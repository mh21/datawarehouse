"""Issues scripts."""
from collections import defaultdict

from cki_lib.logger import get_logger
from django.conf import settings
from django.template import loader

from datawarehouse import models
from datawarehouse import utils

LOGGER = get_logger(__name__)


def update_issue_occurrences_related_checkout(instance, issues_ids):
    """
    Update IssueOccurrence.related_checkout parameter.

    Called on post_add, this function sets the related_checkout parameter
    on the created IssueOccurrence objects.
    """
    if isinstance(instance, models.KCIDBTestResult):
        # For IssueOccurrence in KCIDBTestResult, use KCIDBTest instead
        instance = instance.test

    # The call arguments don't contain the IssueOccurrence so it's
    # necessary to query it with the Issue and the KCIDB object.
    if isinstance(instance, models.KCIDBCheckout):
        related_checkout = instance
        issue_occurrence_filter = {'kcidb_checkout': instance}
    elif isinstance(instance, models.KCIDBBuild):
        related_checkout = instance.checkout
        issue_occurrence_filter = {'kcidb_build': instance}
    elif isinstance(instance, models.KCIDBTest):
        related_checkout = instance.build.checkout
        issue_occurrence_filter = {'kcidb_test': instance}
    else:
        raise Exception('Unhandled signal instance')

    models.IssueOccurrence.objects.filter(
        issue__id__in=issues_ids,
        **issue_occurrence_filter
    ).update(
        related_checkout=related_checkout
    )


def update_issue_occurrences_regression(instance, issues_ids):
    """Set regression=True if the IssueOccurrence is a regression."""
    if isinstance(instance, models.KCIDBTestResult):
        # For IssueOccurrence in KCIDBTestResult, use KCIDBTest instead
        instance = instance.test

    # The call arguments don't contain the IssueOccurrence so it's
    # necessary to query it with the Issue and the KCIDB object.
    # Always retrieve the checkout start time to compare with regression
    # resolve time, as the kernel code and a lot of CKI packages are retrieved
    # at the start of the run, and thus the start time of the object does not
    # reflect when it was possible for a fix to go in.
    if isinstance(instance, models.KCIDBCheckout):
        issue_occurrence_filter = {'kcidb_checkout': instance}
        start_time = instance.start_time
    elif isinstance(instance, models.KCIDBBuild):
        issue_occurrence_filter = {'kcidb_build': instance}
        start_time = instance.checkout.start_time
    elif isinstance(instance, models.KCIDBTest):
        issue_occurrence_filter = {'kcidb_test': instance}
        start_time = instance.build.checkout.start_time
    else:
        raise Exception('Unhandled signal instance')

    issue_occurrence_filter.update({
        'issue__id__in': issues_ids,
        'issue__resolved_at__isnull': False,
    })

    if start_time:
        # Best effort, only compare resolved_at date if the object has one.
        issue_occurrence_filter.update({
            'issue__resolved_at__lt': start_time,
        })

    models.IssueOccurrence.objects.filter(
        **issue_occurrence_filter,
    ).update(
        is_regression=True
    )


def send_regression_notification(instance, issues_ids):
    """Call notify_issue_regression if necessary."""
    if not settings.FF_NOTIFY_ISSUE_REGRESSION:
        return

    if instance.checkout.retrigger:
        # Do not send regression notifications for retriggers
        return

    if isinstance(instance, models.KCIDBTestResult):
        # For IssueOccurrence in KCIDBTestResult, use KCIDBTest instead
        instance = instance.test

    if isinstance(instance, models.KCIDBCheckout):
        issue_occurrence_filter = {'kcidb_checkout': instance}
    elif isinstance(instance, models.KCIDBBuild):
        issue_occurrence_filter = {'kcidb_build': instance}
    elif isinstance(instance, models.KCIDBTest):
        issue_occurrence_filter = {'kcidb_test': instance}
    else:
        raise Exception('Unhandled signal instance')

    issue_occurrences = models.IssueOccurrence.objects.filter(
        issue_id__in=issues_ids,
        **issue_occurrence_filter,
        is_regression=True
    )

    for issue_occurrence in issue_occurrences:
        notify_issue_regression(
            instance,
            issue_occurrence.issue,
        )


def get_issue_regression_notification_recipients(obj, issue):
    """
    Get the recipients for an issue regression.

    Depending on users settings, checkout type and other factors,
    calculate who needs to be notified about this regression.
    """
    recipients = defaultdict(set)

    if submitter := obj.checkout.submitter:
        recipients['to'].add(submitter.email)

    if obj.checkout.scratch:
        # On scratch checkouts, only notify the submitter
        return recipients

    # Filter users with authorization to read the issue
    users = issue.users_read_authorized
    # Exclude users not subscribed to the issue regression
    users = users.exclude(subscriptions__issue_regression_subscribed_at=None)
    # Filter users with authorization to read the object
    users = [u for u in users if u in obj.users_read_authorized]

    recipients['cc'].update(
        u.email for u in users
        if u.subscriptions.issue_regression_visibility == models.SubscriptionVisibility.CC
    )

    recipients['bcc'].update(
        u.email for u in users
        if u.subscriptions.issue_regression_visibility == models.SubscriptionVisibility.BCC
    )

    if isinstance(obj, models.KCIDBTest):
        maintainers_emails = obj.test.maintainers.values_list('email', flat=True)
        recipients['to'].update(maintainers_emails)

    return recipients


def notify_issue_regression(obj, issue):
    """Notify that an issue regression was detected."""
    email_template = loader.get_template('email_issue_regression.html')
    email_context = {
        'issue': issue,
        'obj': obj,
    }

    email_subject = f'{issue.kind.tag} | Issue #{issue.id} regression detected'
    email_message = email_template.render(email_context, {})

    recipients = get_issue_regression_notification_recipients(obj, issue)
    # Sets are not serializable, convert to list.
    recipients = {k: list(v) for k, v in recipients.items()}

    if any(recipients.values()):
        LOGGER.info("notify_issue_regression: issue_id=%d recipients=%s",
                    issue.id, recipients)
        utils.EMAIL_QUEUE.add(
            email_subject, email_message, recipients
        )
