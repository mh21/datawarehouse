"""Add and populate checkout.source_package_name."""
from django.db import migrations, models


def populate(apps, schema_editor):
    """Populate source_package_namepackage from package_name."""
    KCIDBCheckout = apps.get_model('datawarehouse', 'KCIDBCheckout')
    db_alias = schema_editor.connection.alias

    for checkout in KCIDBCheckout.objects.using(db_alias).all():
        checkout.source_package_name = checkout.kcidbbuild_set.values_list('package_name',
                                                                           flat=True).first()
        checkout.save()


class Migration(migrations.Migration):

    dependencies = [
        ('datawarehouse', '0115_issueregex_package_name_match'),
    ]

    operations = [
        migrations.AddField(
            model_name='kcidbcheckout',
            name='source_package_name',
            field=models.CharField(max_length=100, null=True, blank=True)
        ),
        migrations.RunPython(populate),
    ]
