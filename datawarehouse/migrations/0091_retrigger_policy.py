"""Populate retrigger policy related models."""
from django.db import migrations, models


def populate(apps, schema_editor):
    """Populate policies."""
    Group = apps.get_model('auth', 'Group')
    Policy = apps.get_model('datawarehouse', 'Policy')
    db_alias = schema_editor.connection.alias

    group_retrigger_rw = Group.objects.using(db_alias).get_or_create(name='policy_retrigger_rw')[0]

    Policy.objects.using(db_alias).get_or_create(
        name='retrigger',
        defaults={
            'read_group': group_retrigger_rw,
            'write_group': group_retrigger_rw,
        }
    )


class Migration(migrations.Migration):

    dependencies = [
        ('datawarehouse', '0090_queuedtask'),
    ]

    operations = [
        migrations.RunPython(populate),
    ]
