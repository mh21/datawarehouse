"""Populate Issue resolved_at."""
from django.db import migrations, models
from django.utils import timezone


def populate(apps, schema_editor):
    """Populate."""
    Issue = apps.get_model('datawarehouse', 'Issue')
    db_alias = schema_editor.connection.alias

    Issue.objects.using(db_alias).filter(resolved=True).update(
        resolved_on=timezone.now()
    )


class Migration(migrations.Migration):

    dependencies = [
        ('datawarehouse', '0074_issue_resolved_on'),
    ]

    operations = [
        migrations.RunPython(populate),
    ]
