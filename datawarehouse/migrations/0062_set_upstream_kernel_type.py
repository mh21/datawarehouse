"""Populate upstream checkouts."""
from django.db import migrations, models


def populate(apps, schema_editor):
    """Populate policies."""
    GitTree = apps.get_model('datawarehouse', 'GitTree')
    Policy = apps.get_model('datawarehouse', 'Policy')
    db_alias = schema_editor.connection.alias

    public_policy = Policy.objects.using(db_alias).get(name='public')

    GitTree.objects.using(db_alias).filter(
        kcidbcheckout__gitlabjob__pipeline__variables__key='kernel_type',
        kcidbcheckout__gitlabjob__pipeline__variables__value='upstream',
    ).update(
        policy=public_policy
    )


class Migration(migrations.Migration):

    dependencies = [
        ('datawarehouse', '0061_kcidbcheckout_kernel_type'),
    ]

    operations = [
        migrations.RunPython(populate),
    ]
