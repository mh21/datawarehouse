# Generated by Django 4.1.9 on 2023-06-06 14:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('datawarehouse', '0117_alter_provenancecomponent_url'),
    ]

    operations = [
        migrations.AlterField(
            model_name='kcidbbuild',
            name='architecture',
            field=models.IntegerField(blank=True, choices=[(2, 'aarch64'), (3, 'ppc64'), (4, 'ppc64le'), (5, 's390x'), (6, 'x86_64'), (8, 'i686'), (9, 'noarch')], null=True),
        ),
    ]
