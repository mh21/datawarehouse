"""Populate Pipeline's KCIDBCheckout."""
from django.db import migrations, models
from django.db.models import F


def populate(apps, schema_editor):
    """Populate."""
    GitlabJob = apps.get_model('datawarehouse', 'GitlabJob')
    db_alias = schema_editor.connection.alias

    gitlab_jobs = (
        GitlabJob.objects.exclude(kcidb_checkout=None)
        .select_related('pipeline', 'kcidb_checkout')
    )

    for job in gitlab_jobs:
        job.pipeline.kcidb_checkout = job.kcidb_checkout
        job.pipeline.save()


class Migration(migrations.Migration):

    dependencies = [
        ('datawarehouse', '0070_pipeline_kcidb_checkout'),
    ]

    operations = [
        migrations.RunPython(populate),
    ]
