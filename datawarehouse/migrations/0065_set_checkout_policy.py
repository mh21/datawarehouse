"""Populate KCIDBCheckout policies."""
from django.db import migrations, models


def populate(apps, schema_editor):
    """Populate policies."""
    KCIDBCheckout = apps.get_model('datawarehouse', 'KCIDBCheckout')
    Policy = apps.get_model('datawarehouse', 'Policy')
    db_alias = schema_editor.connection.alias

    public_policy = Policy.objects.using(db_alias).get(name='public')
    internal_policy = Policy.objects.using(db_alias).get(name='internal')

    KCIDBCheckout.objects.using(db_alias).filter(kernel_type='u').update(policy=public_policy)
    KCIDBCheckout.objects.using(db_alias).filter(kernel_type='i').update(policy=internal_policy)


class Migration(migrations.Migration):

    dependencies = [
        ('datawarehouse', '0064_kcidbcheckout_policy'),
    ]

    operations = [
        migrations.RunPython(populate),
    ]
