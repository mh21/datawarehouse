"""Populate initial policy related models."""
from django.db import migrations, models


def populate(apps, schema_editor):
    """Populate policies."""
    Group = apps.get_model('auth', 'Group')
    Policy = apps.get_model('datawarehouse', 'Policy')
    db_alias = schema_editor.connection.alias

    group_internal_read = Group.objects.using(db_alias).get_or_create(name='policy_internal_read')[0]
    group_internal_write = Group.objects.using(db_alias).get_or_create(name='policy_internal_write')[0]
    group_public_write = Group.objects.using(db_alias).get_or_create(name='policy_public_write')[0]

    Policy.objects.using(db_alias).get_or_create(
        name='internal',
        defaults={
            'read_group': group_internal_read,
            'write_group': group_internal_write,
        }
    )

    Policy.objects.using(db_alias).get_or_create(
        name='public',
        defaults={
            'read_group': None,
            'write_group': group_public_write,
        }
    )


class Migration(migrations.Migration):

    dependencies = [
        ('datawarehouse', '0056_issue_timestamps'),
    ]

    operations = [
        migrations.RunPython(populate),
    ]
