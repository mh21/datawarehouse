"""Styles file."""
# pylint: disable=too-few-public-methods

from dataclasses import dataclass

from datawarehouse import models


@dataclass
class Style:
    """Encapsulates GUI styles related to task results."""

    icon: str
    icon_big: str
    cell: str
    row: str
    color: str
    text_color: str
    cell_with_text_color: str

    def __init__(self, icon: str, color: str = '', colored_row: bool = False) -> None:
        """Construct a new Style class."""
        table_color = f'table-{color}' if color else ''
        self.text_color = f'text-{color}' if color else ''
        self.icon = f'<i class="{icon} {self.text_color}"></i>'
        self.icon_big = f'<i class="{icon} {self.text_color} fa-2x"></i>'
        self.cell = f'col-1 {table_color}'
        self.row = table_color if colored_row else ''
        self.color = color
        self.cell_with_text_color = f'{self.cell} {self.text_color}'


_STYLE_RUNNING = Style('fas fa-sync', 'info')
_STYLE_SKIP = Style('far fa-window-minimize')
_STYLE_GOOD = Style('fas fa-check', 'success')
_STYLE_BAD = Style('fas fa-times', 'danger', colored_row=True)
_STYLE_FAIL = Style('fas fa-times', 'danger', colored_row=True)
_STYLE_FAIL_WAIVED = Style('fas fa-times', 'warning', colored_row=True)
_STYLE_ERROR = Style('fas fa-fire-alt', 'danger', colored_row=True)
_STYLE_ERROR_WAIVED = Style('fas fa-fire-alt', 'warning', colored_row=True)


def get_style(level: str, waived: bool = False) -> Style:
    """Return style values for template rendering."""
    if level == 'Running' or level is None:
        return _STYLE_RUNNING
    if level == models.ResultEnum.SKIP:
        return _STYLE_SKIP
    if level == models.ResultEnum.PASS:
        return _STYLE_GOOD
    if level == models.ResultEnum.ERROR:
        return _STYLE_ERROR if not waived else _STYLE_ERROR_WAIVED
    if level == models.ResultEnum.FAIL:
        return _STYLE_FAIL if not waived else _STYLE_FAIL_WAIVED
    return _STYLE_BAD
