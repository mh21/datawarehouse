"""Get attribute templatetags."""
from django import template


def getattribute(value, arg):
    """Get an attribute of an object dynamically from a string name."""
    if hasattr(value, arg):
        return getattr(value, arg)
    if type(value) == dict:
        return value[arg]

    return ''


register = template.Library()
register.filter('getattribute', getattribute)
