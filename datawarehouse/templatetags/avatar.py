"""Avatar template tags."""
import hashlib
import urllib

from django import template
from django.conf import settings

register = template.Library()


@register.filter
def avatar_url(email, size=40):
    """Return avatar url for given user."""
    if not settings.FF_EXTERNAL_AVATAR_ENABLED:
        return settings.DEFAULT_USER_IMAGE

    user_hash = hashlib.md5(email.lower().encode('utf-8')).hexdigest()
    params = urllib.parse.urlencode({
        'd': settings.EXTERNAL_AVATAR_DEFAULT,
        's': size
    })
    return f"{settings.EXTERNAL_AVATAR_URL}/{user_hash}?{params}"
