# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (c) 2018-2019 Red Hat, Inc.
"""Issue models file."""

from django.conf import settings
from django.db import models
from django.urls import reverse
from django_prometheus.models import ExportModelOperationsMixin as EMOM

from datawarehouse import models as dw_models

from .utils import CreatedStampedModel
from .utils import GenericDescriptionManager
from .utils import Manager
from .utils import Model
from .utils import StampedModel


class IssueKind(EMOM('issue_kind'), models.Model):
    """Model for IssueKind."""

    description = models.CharField(max_length=200)
    tag = models.CharField(max_length=20)
    color = models.CharField(max_length=7, default='#dc3545')
    kernel_code_related = models.BooleanField(default=False)

    objects = GenericDescriptionManager()

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.description}'


class Issue(EMOM('issue'), Model, StampedModel):
    """Model for Issue."""

    kind = models.ForeignKey(IssueKind, on_delete=models.CASCADE, null=True)
    description = models.TextField()
    ticket_url = models.URLField(unique=True)
    resolved_at = models.DateTimeField(blank=True, null=True)

    policy = models.ForeignKey(dw_models.Policy, on_delete=models.PROTECT,
                               null=True, blank=True)
    # Automatically set the policy as 'public' if there is a 'public' object associated
    policy_auto_public = models.BooleanField(default=False)

    objects = Manager()

    class Meta:
        """Meta."""

        ordering = ('-id', )

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.description}'

    @property
    def resolved(self):
        """Return True if the Issue is resolved."""
        return bool(self.resolved_at)

    @property
    def web_url(self):
        """Return the URL to this object in the web interface."""
        return settings.DATAWAREHOUSE_URL + reverse('views.issue.get', args=[self.id])

    def get_checkouts(self, request):
        """
        Get KCIDBCheckout for this issue.

        Only return objects authorized for the user doing the request.
        """
        return (
            dw_models.KCIDBCheckout.objects
            .filter_authorized(request)
            .filter(
                iid__in=(
                    self.issueoccurrence_set.values_list('related_checkout__iid', flat=True)
                )
            )
            .select_related('tree')
            .order_by(
                models.F('start_time').desc(nulls_last=True)
            )
        )


class IssueRegex(EMOM('issue_regex'), Model, StampedModel):
    """Model for IssueRegex."""

    issue = models.ForeignKey(Issue, on_delete=models.CASCADE, related_name='issue_regexes')
    text_match = models.TextField()
    file_name_match = models.CharField(max_length=200, null=True, default=None)
    test_name_match = models.CharField(max_length=200, null=True, default=None)
    architecture_match = models.CharField(max_length=100, null=True, default=None)
    tree_match = models.CharField(max_length=100, null=True, default=None)
    kpet_tree_name_match = models.CharField(max_length=100, null=True, default=None)
    package_name_match = models.CharField(max_length=100, null=True, default=None)

    path_to_policy = 'issue__policy'

    objects = Manager()

    class Meta:
        """Meta."""

        ordering = ('-id', )

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.kpet_tree_name_match} - {self.tree_match} - {self.architecture_match} \
        - {self.package_name_match} - {self.test_name_match} - {self.file_name_match} \
        - {self.text_match}'


class IssueOccurrence(EMOM('issue_occurrence'), Model, CreatedStampedModel):
    """Model for IssueOccurrence."""

    issue = models.ForeignKey('Issue', on_delete=models.CASCADE)
    kcidb_checkout = models.ForeignKey('KCIDBCheckout', null=True, on_delete=models.CASCADE)
    kcidb_build = models.ForeignKey('KCIDBBuild', null=True, on_delete=models.CASCADE)
    kcidb_test = models.ForeignKey('KCIDBTest', null=True, on_delete=models.CASCADE)
    kcidb_testresult = models.ForeignKey('KCIDBTestResult', null=True, on_delete=models.CASCADE)
    is_regression = models.BooleanField(default=False)

    # Store the affected checkout when the occurrence happened on a child object (build or test).
    # This attribute is populated by signal_receivers.issue_occurrence_assigned post_add signal.
    related_checkout = models.ForeignKey('KCIDBCheckout', null=True, on_delete=models.CASCADE,
                                         related_name='related_issue_ocurrences')

    path_to_policy = 'issue__policy'
    objects = Manager()

    class Meta:
        """Meta."""

        ordering = ('-id', )

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.id} - {self.issue}'
