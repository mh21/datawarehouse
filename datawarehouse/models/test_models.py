# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (c) 2018-2019 Red Hat, Inc.

# pylint: disable=too-few-public-methods

"""Models related to the test stage."""

from django.conf import settings
from django.db import models
from django_prometheus.models import ExportModelOperationsMixin as EMOM

from datawarehouse.models import utils


class TestMaintainer(EMOM('test_maintainer'), models.Model):
    """Model for TestMaintainer."""

    name = models.CharField(max_length=50)
    gitlab_username = models.CharField(max_length=50, blank=True, null=True)
    email = models.EmailField()

    objects = utils.GenericNameManager()

    class Meta:
        """Metadata."""

        ordering = ('name',)

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.name} <{self.email}>'

    @classmethod
    def create_from_dict(cls, data):
        """Create Maintainer from address."""
        maintainer = cls.objects.get_or_create(
            email=data['email'],
            defaults={
                'name': data.get('name', ''),
                'gitlab_username': data.get('gitlab'),
            }
        )[0]

        # Detect changes to avoid unnecesary SQL queries
        # https://code.djangoproject.com/ticket/27335
        changed = (data.get('name', ''), data.get('gitlab')) != (maintainer.name, maintainer.gitlab_username)
        if changed:
            maintainer.name = data.get('name', '')
            maintainer.gitlab_username = data.get('gitlab')
            maintainer.save()

        return maintainer

    @property
    def gitlab_profile_url(self):
        """
        Return URL to the maintainer Gitlab profile.

        Returns None if settings.GITLAB_URL is not set or the maintainer
        has no gitlab_username set.
        """
        if settings.GITLAB_URL and self.gitlab_username:
            return f'{settings.GITLAB_URL}/{self.gitlab_username}'
        return None


class TestManager(utils.Manager):
    """Natural key for Test."""

    def get_by_natural_key(self, name, fetch_url):
        """Lookup the object by the natural key."""
        return self.get(name=name, fetch_url=fetch_url)


class Test(EMOM('test'), utils.Model):
    """Model for Test."""

    name = models.CharField(max_length=200)
    fetch_url = models.URLField(blank=True, null=True)
    maintainers = models.ManyToManyField(TestMaintainer)
    universal_id = models.CharField(max_length=200, null=True)

    objects = TestManager()

    path_to_policy = 'kcidbtest__build__checkout__policy'

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.name}'

    def natural_key(self):
        """Return the natural key."""
        return (self.name, self.fetch_url)

    @property
    def confidence(self):
        """Confidence index."""
        result = (self.testrun_set
                  .annotate(passed_count=models.Count('id', filter=models.Q(passed=True)))
                  .aggregate(confidence=models.Avg('passed_count')))['confidence']
        return result if result is not None else 1

    @property
    def name_sanitized(self):
        """Turn a string into a path-compatible name."""
        return "".join(
            char if char.isalnum() else '_' for char in self.name
        )

    def set_maintainers(self, maintainers):
        """
        Set test maintainers.

        Arguments:
            - maintainers: List of dictionaries with the maintainer details.
        """
        self.maintainers.set(
            [
                TestMaintainer.create_from_dict(maintainer)
                for maintainer in maintainers
            ]
        )

    @classmethod
    def get_and_update(cls, name, universal_id, fetch_url, maintainers=None):
        """
        Create Test and update fields if necessary.

        Arguments:
            - name: Test name.
            - universal_id: Test path or universal id.
            - maintainers: List of dictionaries with the maintainer details.
        """
        test = cls.objects.get_or_create(
            name=name,
            defaults={
                'universal_id': universal_id,
                'fetch_url': fetch_url,
            }
        )[0]

        # Detect changes to avoid unnecesary SQL queries
        # https://code.djangoproject.com/ticket/27335
        changed = (
            (universal_id is not None and universal_id != test.universal_id) |
            (fetch_url is not None and fetch_url != test.fetch_url)
        )
        if changed:
            test.universal_id = universal_id or test.universal_id
            test.fetch_url = fetch_url or test.fetch_url
            test.save()

        if maintainers:
            test.set_maintainers(maintainers)

        return test


class BeakerResourceManager(utils.Manager):
    """Natural key for BeakerResource."""

    def get_by_natural_key(self, fqdn):
        """Lookup the object by the natural key."""
        return self.get(fqdn=fqdn)


class BeakerResource(EMOM('beaker_resource'), utils.Model):
    """Model for BeakerResource."""

    fqdn = models.CharField(max_length=100, blank=True, null=True)

    path_to_policy = 'kcidbtest__build__checkout__policy'

    objects = BeakerResourceManager()

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.fqdn}'

    def natural_key(self):
        """Return the natural key."""
        return (self.fqdn, )

    @property
    def confidence(self):
        """Confidence index."""
        result = (self.beakertestrun_set
                  .annotate(passed_count=models.Count('id', filter=models.Q(passed=True)))
                  .aggregate(confidence=models.Avg('passed_count')))['confidence']
        return result if result is not None else 1


class ResultEnum(models.TextChoices):
    """Enumeration of the possible test results."""

    FAIL = 'F', 'FAIL'
    ERROR = 'E', 'ERROR'
    PASS = 'P', 'PASS'
    DONE = 'D', 'DONE'
    SKIP = 'S', 'SKIP'
    NEW = 'N', 'NEW'
