"""LDAP methods."""
from contextlib import contextmanager
import re

from django.conf import settings
import ldap3


@contextmanager
def connection():
    """Get LDAPConnection as context manager."""
    conn = LDAPConnection()
    conn.connect()
    try:
        yield conn
    finally:
        conn.disconnect()


class LDAPConnection:
    """LDAP Server and Connection."""

    def __init__(self):
        """Initialize."""
        self.server = ldap3.Server(settings.LDAP_CONFIG['server_url'])
        self.connection = None

    def connect(self):
        """Connect to ldap server."""
        self.connection = ldap3.Connection(self.server, auto_bind=True)
        self.connection.bind()

    def disconnect(self):
        """Unbind connection."""
        if not self.connection:
            return
        self.connection.unbind()
        self.connection = None

    def get_users(self, ldap_dn):
        """
        Get users for a given DN.

        Returns a list of (uid, email) for each member of ldap_dn.
        """
        self.connection.search(
            search_base=settings.LDAP_CONFIG['base_search'],
            search_filter=f'(&(objectClass=person)(memberOf={ldap_dn}))',
            search_scope=ldap3.SUBTREE,
            attributes=[settings.LDAP_CONFIG['members_field'], settings.LDAP_CONFIG['email_field']],
            size_limit=0
        )

        return re.findall(
            fr"DN:\s+uid=(\w+).*\n\s*{settings.LDAP_CONFIG['email_field']}: (\S+)",
            str(self.connection.entries)
        )
